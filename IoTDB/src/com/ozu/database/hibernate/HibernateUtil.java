package com.ozu.database.hibernate;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ozu.generator.PojoGenerator;

public class HibernateUtil {

	private static final String SESSION_FACTORY = "session-factory";
	private static final String CLASS = "class";
	private static final String MAPPING = "mapping";
	private static final String ROOT = "src/";
	private static final String CONFIG_FILE_PATH = ROOT + "hibernate.cfg.xml";
	private static final String CONFIG_BOILERPLATE_FILE_PATH = ROOT + "HibernateConfigBoilerplate.xml";
	private static final String IOT_DB_PATH = "/Users/xx/Documents/workspace/IoT/IoTDB";

	private static final String DOC_TYPE = "\n<!DOCTYPE hibernate-configuration PUBLIC\n"
			+ "\"-//Hibernate/Hibernate Configuration DTD 3.0//EN\"\n"
			+ "\"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd\">\n";

	private static SessionFactory sessionFactory;

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure(new File(IOT_DB_PATH + "/src/hibernate.cfg.xml"));
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			return sessionFactory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

	public static void createConfigXml() {
		try {
			File fXmlFile = new File(CONFIG_BOILERPLATE_FILE_PATH);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(SESSION_FACTORY);

			List<String> generatedConceptClasses = PojoGenerator.getInstance().getGeneratedConceptClasses();
			for (String clazzName : generatedConceptClasses) {
				Element element = doc.createElement(MAPPING);
				element.setAttribute(CLASS, PojoGenerator.PACKAGE_NAME_CONCEPT + clazzName);
				nList.item(0).appendChild(element);
			}

			List<String> generatedPropertyClasses = PojoGenerator.getInstance().getGeneratedPropertyClasses();
			for (String clazzName : generatedPropertyClasses) {
				Element element = doc.createElement(MAPPING);
				element.setAttribute(CLASS, PojoGenerator.PACKAGE_NAME_OBJECT_PROPERTY + clazzName);
				nList.item(0).appendChild(element);
			}

			List<String> generatedDataClasses = PojoGenerator.getInstance().getGeneratedDataPropertyClasses();
			for (String clazzName : generatedDataClasses) {
				Element element = doc.createElement(MAPPING);
				element.setAttribute(CLASS, PojoGenerator.PACKAGE_NAME_DATA_PROPERTY + clazzName);
				nList.item(0).appendChild(element);
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(CONFIG_FILE_PATH));

			transformer.transform(source, result);

			/**
			 * Add doc type via string
			 */

			File file = new File(CONFIG_FILE_PATH);
			String lines = FileUtils.readFileToString(file);

			lines = lines.substring(0, lines.indexOf("?>") + 2) + DOC_TYPE + lines.substring(lines.indexOf("?>") + 3);

			FileUtils.writeStringToFile(file, lines);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.ozu.generator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import com.ozu.ont.pojo.boilerplate.interfaces.IThing;
import com.sun.codemodel.ClassType;
import com.sun.codemodel.JAnnotationUse;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

public class PojoGenerator {
	
	private static final String PATH = "src";

	public static final String PACKAGE_NAME_CONCEPT = "com.ozu.ont.pojo.generated.concept.";
	public static final String PACKAGE_NAME_OBJECT_PROPERTY = "com.ozu.ont.pojo.generated.property.object.";
	public static final String PACKAGE_NAME_DATA_PROPERTY = "com.ozu.ont.pojo.generated.property.data.";
	
	private static PojoGenerator instance = new PojoGenerator();
	
	private HashMap<String, JDefinedClass> generatedConceptClasses = new HashMap<String, JDefinedClass>();
	private HashMap<String, JDefinedClass> generatedObjectPropertyClasses = new HashMap<String, JDefinedClass>();;
	private HashMap<String, JDefinedClass> generatedDataPropertyClasses = new HashMap<String, JDefinedClass>();;
	
	
	private JCodeModel codeModel;

	public static PojoGenerator getInstance(){
		return instance;
	}
	
	private PojoGenerator() {
		codeModel = new JCodeModel();
	}

	/**
	 * Create a class that represents a concept.
	 * @param clazzName
	 * @return returns the newly created class.
	 */
	public JDefinedClass createConceptClass(String clazzName){
		try {
			if(generatedConceptClasses.containsKey(clazzName))
			{
				return generatedConceptClasses.get(clazzName);
			}
			JDefinedClass jClass = codeModel._class(PACKAGE_NAME_CONCEPT + clazzName, ClassType.CLASS);
			jClass.annotate(Entity.class);
			jClass._extends(Thing.class);
			
			JMethod constructor = jClass.constructor(JMod.PUBLIC);
			constructor.param(codeModel.ref(String.class), "objectName");
			
			StringBuilder expression = new StringBuilder();
			expression.append("super(objectName);");
			constructor.body().directStatement(expression.toString());
			
			/**
			 * Default Constructor
			 */
			JMethod defaultConstructor = jClass.constructor(JMod.PUBLIC);
			defaultConstructor.body().directStatement("super();");
			
			generatedConceptClasses.put(clazzName, jClass);
			
			return jClass;
		} catch (JClassAlreadyExistsException e) { 
			throw new RuntimeException(e.getMessage());// Should never be here
		}
	}
	
	/**
	 * Create a class that represents an object property.
	 * @param clazzName
	 * @return returns the newly created class.
	 */
	@SuppressWarnings("unused")
	public JDefinedClass createObjectPropertyClass(String clazzName) {
		try {
			if(generatedObjectPropertyClasses.containsKey(clazzName))
			{
				return generatedObjectPropertyClasses.get(clazzName);
			}
			JDefinedClass jClass = codeModel._class(PACKAGE_NAME_OBJECT_PROPERTY + clazzName, ClassType.CLASS);
			jClass.annotate(Entity.class);
			jClass._extends(ObjectProperty.class);
			
//			JFieldVar predicateField = jClass.field(JMod.PROTECTED, codeModel.ref(String.class), "predicate");
			
			JFieldVar subjectField = jClass.field(JMod.PROTECTED, codeModel.ref(Integer.class), "subjectId");
//			JAnnotationUse subjectAnnotate = subjectField.annotate(OneToOne.class);
//			subjectAnnotate.param("cascade", CascadeType.ALL);
			
			JFieldVar objectField = jClass.field(JMod.PROTECTED, codeModel.ref(Integer.class), "objectId");
//			JAnnotationUse objectAnnotate = objectField.annotate(OneToOne.class);
//			objectAnnotate.param("cascade", CascadeType.ALL);
			
			JMethod constructor = jClass.constructor(JMod.PUBLIC);
			constructor.param(codeModel.ref(String.class), "predicate");
			constructor.param(codeModel.ref(Thing.class), "subject");
			constructor.param(codeModel.ref(Thing.class), "object");
			
			jClass.constructor(JMod.PUBLIC);
			
			StringBuilder expression = new StringBuilder();
			expression.append("this.predicate = predicate;\n"
					+ "this.subjectId = subject.getId();\n"
					+ "this.objectId = object.getId();\n"
					+ "super.subject = subject;\n"
					+ "super.object = object;\n");
			constructor.body().directStatement(expression.toString());
			
			JMethod getPredicate = jClass.method(JMod.PUBLIC, codeModel.ref(String.class), "getPredicate");
			JMethod getSubject = jClass.method(JMod.PUBLIC, codeModel.ref(IThing.class), "getSubject");
			JMethod getObject = jClass.method(JMod.PUBLIC, codeModel.ref(IThing.class), "getObject");
			
			getPredicate.body().directStatement("return predicate;");
			getSubject.body().directStatement("return subject;");
			getObject.body().directStatement("return object;");
			
			generatedObjectPropertyClasses.put(clazzName, jClass);
			return jClass;
		} catch (JClassAlreadyExistsException e) {
			throw new RuntimeException(e.getMessage());// Should never be here
		}
		
	}
	
	/**
	 * Create a class that represents an object property.
	 * @param clazzName
	 * @return returns the newly created class.
	 */
	@SuppressWarnings("unused")
	public JDefinedClass createDataPropertyClass(String clazzName) {
		try {
			if(generatedDataPropertyClasses.containsKey(clazzName))
			{
				return generatedDataPropertyClasses.get(clazzName);
			}
			JDefinedClass jClass = codeModel._class(PACKAGE_NAME_DATA_PROPERTY + clazzName, ClassType.CLASS);
			jClass.annotate(Entity.class);
			jClass._extends(DataProperty.class);
			
//			JFieldVar predicateField = jClass.field(JMod.PROTECTED, codeModel.ref(String.class), "predicate");
			
			JFieldVar subjectField = jClass.field(JMod.PROTECTED, codeModel.ref(Integer.class), "subjectId");
//			JAnnotationUse subjectAnnotate = subjectField.annotate(OneToOne.class);
//			subjectAnnotate.param("cascade", CascadeType.ALL);
			
			JFieldVar dataField = jClass.field(JMod.PROTECTED, codeModel.ref(String.class), "data");
			
			JFieldVar dataTypeField = jClass.field(JMod.PROTECTED, codeModel.ref(EDataType.class), "type");
//			JAnnotationUse objectAnnotate = objectField.annotate(OneToOne.class);
//			objectAnnotate.param("cascade", CascadeType.ALL);
			
			JMethod constructor = jClass.constructor(JMod.PUBLIC);
			constructor.param(codeModel.ref(String.class), "predicate");
			constructor.param(codeModel.ref(Thing.class), "subject");
			constructor.param(codeModel.ref(String.class), "data");
			constructor.param(codeModel.ref(EDataType.class), "type");
			
			jClass.constructor(JMod.PUBLIC);
			
			StringBuilder expression = new StringBuilder();
			expression.append("this.predicate = predicate;\n"
					+ "this.subjectId = subject.getId();\n"
					+ "this.data = data;\n"
					+ "this.type = type;\n"
					+ "super.subject = subject;\n"
					+ "super.data = data;\n"
					+ "super.type = type;\n");
			constructor.body().directStatement(expression.toString());
			
			JMethod getPredicate = jClass.method(JMod.PUBLIC, codeModel.ref(String.class), "getPredicate");
			JMethod getSubject = jClass.method(JMod.PUBLIC, codeModel.ref(IThing.class), "getSubject");
			JMethod getData = jClass.method(JMod.PUBLIC, codeModel.ref(String.class), "getData");
			JMethod getType = jClass.method(JMod.PUBLIC, codeModel.ref(EDataType.class), "getType");
			
			getPredicate.body().directStatement("return predicate;");
			getSubject.body().directStatement("return subject;");
			getData.body().directStatement("return data;");
			getType.body().directStatement("return type;");
			
			JAnnotationUse annotate = getType.annotate(Enumerated.class);
			annotate.param("value", EnumType.STRING);
			
			
			generatedDataPropertyClasses.put(clazzName, jClass);
			return jClass;
		} catch (JClassAlreadyExistsException e) {
			throw new RuntimeException(e.getMessage());// Should never be here
		}
		
	}
	
	/**
	 * Creates add method for an object property
	 * public void add_'methodName'('argumentType' value)
	 * public 'argumentType' get_'MethodName' ()
	 * @param clazzName
	 * @param methodName
	 * @param argumentType
	 */
	@SuppressWarnings("unused")//create concept class if not created
	public void createMethod(String clazzName, String methodName, String argumentType) {
		JDefinedClass clazz = getConceptClass(clazzName);
		JDefinedClass argumentClazz = getConceptClass(argumentType);
		JDefinedClass objectPropertyClass = getObjectPropertyClass(methodName);
		
		JMethod addMethod = clazz.method(JMod.PUBLIC, JType.parse(codeModel, "void") , "add_"+methodName);
		JVar param = addMethod.param(codeModel.ref(argumentType), "arg");
		
		
		JInvocation newInstance = JExpr._new(codeModel.ref(PACKAGE_NAME_OBJECT_PROPERTY+methodName));
		newInstance.arg(JExpr._this());
		newInstance.arg(param);
		
		JInvocation invocation = JExpr._super().ref("objectProperties").invoke("add").arg(newInstance);
		
		addMethod.body().add(invocation);
	}

	/**
	 * @param conceptClassName
	 * @return
	 */
	public JDefinedClass getConceptClass(String conceptClassName){
		if(generatedConceptClasses.containsKey(conceptClassName) == false){
			createConceptClass(conceptClassName);
		}
		return generatedConceptClasses.get(conceptClassName);
	}
	
	/**
	 * @param conceptClassName
	 * @return
	 */
	public JDefinedClass getObjectPropertyClass(String propertyClassName){
		if(generatedConceptClasses.containsKey(propertyClassName) == false){
			createObjectPropertyClass(propertyClassName);
		}
		return generatedConceptClasses.get(propertyClassName);
	}
	
	/**
	 * Write all classes to a file
	 */
	public void writeAll(){
		try {
			codeModel.build(new File("./"+PATH));
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());		
		}
	}
	
	public List<String> getGeneratedConceptClasses() {
		LinkedList<String> list = new LinkedList<String>();
		
		Set<Entry<String,JDefinedClass>> set = generatedConceptClasses.entrySet();
		for (Entry<String, JDefinedClass> entry : set) {
			String clazzName = entry.getKey();
			list.add(clazzName);
		}
		
		return list;
	}
	
	public List<String> getGeneratedPropertyClasses() {
		LinkedList<String> list = new LinkedList<String>();
		
		Set<Entry<String,JDefinedClass>> set = generatedObjectPropertyClasses.entrySet();
		for (Entry<String, JDefinedClass> entry : set) {
			String clazzName = entry.getKey();
			list.add(clazzName);
		}
		
		return list;
	}
	
	public List<String> getGeneratedDataPropertyClasses() {
		LinkedList<String> list = new LinkedList<String>();
		
		Set<Entry<String,JDefinedClass>> set = generatedDataPropertyClasses.entrySet();
		for (Entry<String, JDefinedClass> entry : set) {
			String clazzName = entry.getKey();
			list.add(clazzName);
		}
		
		return list;
	}
}

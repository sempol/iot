package com.ozu.generator.crawler;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Property;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.search.EntitySearcher;

import com.google.common.collect.Multimap;
import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.database.hibernate.SandboxUtil;
import com.ozu.generator.PojoGenerator;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import com.ozu.ont.pojo.generated.concept.Device;
import com.ozu.ont.pojo.generated.concept.Door;
import com.ozu.ont.pojo.generated.concept.Doorbell;
import com.ozu.ont.pojo.generated.concept.DoorbellOutput;
import com.ozu.ont.pojo.generated.concept.Event;
import com.ozu.ont.pojo.generated.concept.Flat;
import com.ozu.ont.pojo.generated.concept.HumidityUnit;
import com.ozu.ont.pojo.generated.concept.Location;
import com.ozu.ont.pojo.generated.concept.Mother;
import com.ozu.ont.pojo.generated.concept.Person;
import com.ozu.ont.pojo.generated.concept.Room;
import com.ozu.ont.pojo.generated.concept.SoundNotification;
import com.ozu.ont.pojo.generated.concept.Speaker;
import com.ozu.ont.pojo.generated.concept.TemperatureUnit;
import com.ozu.ont.pojo.generated.concept.VisualNotification;
import com.ozu.ont.pojo.generated.property.object.belongsTo;
import com.ozu.ont.pojo.generated.property.object.canBeNotifiedWith;
import com.ozu.ont.pojo.generated.property.object.canNotifyWith;
import com.ozu.ont.pojo.generated.property.object.disabledNotificationType;
import com.ozu.ont.pojo.generated.property.object.inRoom;
import com.ozu.ont.pojo.generated.property.object.locatedIn;
import com.ozu.ont.pojo.generated.property.object.producedBy;
import com.ozu.ont.pojo.generated.property.object.residesIn;

public class Crawler {

	private static Crawler instance = new Crawler();

	private String ontPath = "src/sspn-ql-rdf.owl";

	private OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	private OWLOntology ont = null;

	public static Crawler getInstance() {
		return instance;
	}

	private Crawler() {
		try {
			ont = manager.loadOntologyFromOntologyDocument(new File(ontPath));
		} catch (OWLOntologyCreationException e) {
			System.out.println("Ontology Creation Exception: " + e.getMessage());
		}
	}

	public void init() {
		Set<OWLClass> classes = ont.getClassesInSignature();
		for (OWLClass owlClass : classes) {
			if (!owlClass.isOWLThing())
				createConceptClass(owlClass);
		}

		Set<OWLObjectProperty> properties = ont.getObjectPropertiesInSignature();
		for (OWLObjectProperty owlObjectProperty : properties) {
			createObjectPropertyClasses(owlObjectProperty);
		}

		Set<OWLDataProperty> data = ont.getDataPropertiesInSignature();
		for (OWLDataProperty owlDataProperty : data) {
			createDataPropertyClass(owlDataProperty);
		}
		// TODO

		/**
		 * Create Individuals
		 */

		int cnt = 0;
		HashMap<String, Integer> cache = new HashMap<String, Integer>();
		/* TODO First create config file then call this part */
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		System.out.println("Class#: " + ont.getClassesInSignature().size());
		System.out.println("ObjectProperties#: " + ont.getObjectPropertiesInSignature().size());
		System.out.println("DataProperties#: " + ont.getDataPropertiesInSignature().size());
		
		int assertionCount = 0;
		for(OWLClass owlClass : ont.getClassesInSignature()){
			System.out.println("Processing class: " + owlClass.getIRI().getShortForm());
			Set<OWLClassAssertionAxiom> classAssertionAxioms = ont.getClassAssertionAxioms(owlClass);
			for (OWLClassAssertionAxiom c : classAssertionAxioms){
				String name = c.getIndividual().asOWLNamedIndividual().getIRI().getShortForm();
				if (cache.containsKey(name)){
					Thing.addIndividualToSession(cache.get(name), owlClass.getIRI().getShortForm(), session);
				}else {
					int id = Thing.addIndividualToSessionIfNotExists(name, owlClass.getIRI().getShortForm(), session);
					cache.put(name, new Integer(id));
				}
			}
			assertionCount += classAssertionAxioms.size();
			System.out.println("Class: " + owlClass.getIRI().getShortForm() + " done!");
			System.out.println("Resetting session...");
			session.getTransaction().commit();
			session.close();
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			System.out.println("Reset is done!");
		}
		
		System.out.println("Type Assertion Count: " + assertionCount);
		
		for (OWLNamedIndividual individual : ont.getIndividualsInSignature()) {

			if (cnt % 100 == 0){
				System.out.println("Processed individual count: " + cnt);
				session.getTransaction().commit();
				session.close();
				session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
			}
			
			Integer id = cache.get(individual.getIRI().getShortForm());
			if (id == null || id == -1){
				id = Thing.addIndividualToSessionIfNotExists(individual.getIRI().getShortForm(), "Thing", session);
				cache.put(individual.getIRI().getShortForm(), id);
			}

			for (OWLObjectPropertyAssertionAxiom objProp : ont.getObjectPropertyAssertionAxioms(individual)){
				String obj = objProp.getObject().asOWLNamedIndividual().getIRI().getShortForm();
				String pred = objProp.getProperty().asOWLObjectProperty().getIRI().getShortForm();
				
				Integer object = cache.get(obj);
				if (object == null || object == -1){
					object = Thing.addIndividualToSessionIfNotExists(obj, "Thing", session);
					cache.put(obj, object);
				}
					
				ObjectProperty.addObjectPropertyToSession(pred, id, object, session);
			}
			
			for(OWLDataPropertyAssertionAxiom dataProp: ont.getDataPropertyAssertionAxioms(individual)){
				String pred = dataProp.getProperty().asOWLDataProperty().getIRI().getShortForm();
				OWLLiteral value = dataProp.getObject();
				
				EDataType t = EDataType.STRING;
				if (value.getDatatype().toString().contains("integer"))
					t = EDataType.LONG;
				else if (value.getDatatype().toString().contains("decimal"))
					t = EDataType.DOUBLE;

				DataProperty.addDataPropertyToSession(id, pred, value.getLiteral(), t, session);
			}
			
			cnt++;
		}
		
		session.getTransaction().commit();
		session.close();
	}

	public void end() {
		PojoGenerator.getInstance().writeAll();
		HibernateUtil.createConfigXml();
		SandboxUtil.createConfigXml();
	}

	public void addTestData() {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();

			 Location location = new Location("location1");
			 session.save(location);

			 Event event = new Event("doorbellEvent");
			 session.save(event);

			 Event frontdoorbellevent = new Event("frontDoorBellEvent");
			 session.save(frontdoorbellevent);

			 Flat flat = new Flat("flat1");
			 session.save(flat);

			 Door door = new Door("door");
			 session.save(door);
			 Device tv = new Device("tv");
			 session.save(tv);
			 Doorbell doorbell = new Doorbell("frontDoorBell");
			 session.save(doorbell);
			 DoorbellOutput output = new DoorbellOutput("someoneAtFrontDoor");
			 session.save(output);
			 SoundNotification sound = new SoundNotification("sound");
			 session.save(sound);
			
			 SoundNotification sound2 = new SoundNotification("sound2");
			 session.save(sound2);
			 Person person = new Person("person2");
			 session.save(person);
			 Mother mother = new Mother("person1");
			 session.save(mother);
			 Speaker speaker = new Speaker("speaker");
			 session.save(speaker);
			 Speaker speaker2 = new Speaker("speaker2");
			 session.save(speaker2);
			 Room room = new Room("room1");
			 session.save(room);
			 VisualNotification visual = new VisualNotification("visual");
			 session.save(visual);

			TemperatureUnit celsius = new TemperatureUnit("celsius");
			session.save(celsius);

			TemperatureUnit kelvin = new TemperatureUnit("kelvin");
			session.save(kelvin);

			HumidityUnit rh = new HumidityUnit("RH");
			session.save(rh);

			 producedBy producedby = new producedBy("producedBy",	frontdoorbellevent, doorbell);
			 session.save(producedby);

			 canBeNotifiedWith canbenotified = new canBeNotifiedWith("canbenotifiedwith", person, sound2);
			 session.save(canbenotified);
			 canBeNotifiedWith canbenotified2 = new
			 canBeNotifiedWith("canbenotifiedwith", mother, sound);
			 session.save(canbenotified2);
			 canBeNotifiedWith canbenotified3 = new
			 canBeNotifiedWith("canbenotifiedwith", mother, visual);
			 session.save(canbenotified3);
			 disabledNotificationType disable = new
			 disabledNotificationType("disabled", mother, sound);
			 session.save(disable);
			 locatedIn locIn = new locatedIn("locatedin", mother, location);
			 session.save(locIn);
			 residesIn resides = new residesIn("residesIn", mother, flat);
			 session.save(resides);
			 canNotifyWith canNotify = new canNotifyWith("canNotifyWith", tv,
			 visual);
			 session.save(canNotify);
			 inRoom inroom = new inRoom("inRoom", tv, room);
			 session.save(inroom);
			 canNotifyWith canNotify2 = new canNotifyWith("canNotifyWith",
			 speaker, sound);
			 session.save(canNotify2);
			
			 inRoom inroom2 = new inRoom("inRoom", speaker, room);
			 session.save(inroom2);

			 belongsTo belong = new belongsTo("belongsTo", room, flat);
			 session.save(belong);

			 locatedIn locInR = new locatedIn("locatedIn", room, location);
			 session.save(locInR);

			session.getTransaction().commit();
			session.close();

			Session sandbox = SandboxUtil.getSessionFactory().openSession();
			sandbox.beginTransaction();
			sandbox.getTransaction().commit();
			sandbox.close();

			System.out.println("Done");
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void createDataPropertyClass(OWLDataProperty owlDataProperty) {
		String clazzName = owlDataProperty.getIRI().getShortForm();
		PojoGenerator.getInstance().createDataPropertyClass(clazzName);
	}

	private void createObjectPropertyClasses(OWLObjectProperty owlObjectProperty) {
		String clazzName = owlObjectProperty.getIRI().getShortForm();
		PojoGenerator.getInstance().createObjectPropertyClass(clazzName);
	}

	private void createConceptClass(OWLClass owlClass) {
		String clazzName = owlClass.getIRI().getShortForm();
		PojoGenerator.getInstance().createConceptClass(clazzName);
	}

	public static void main(String[] args) {
		Crawler.getInstance().init();
		Crawler.getInstance().end();
		Crawler.getInstance().addTestData();
	}
}

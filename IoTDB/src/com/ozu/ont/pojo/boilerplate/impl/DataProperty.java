package com.ozu.ont.pojo.boilerplate.impl;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import com.ozu.ont.pojo.boilerplate.interfaces.IDataProperty;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class DataProperty implements IDataProperty {

	@Id
	@GeneratedValue
	protected int id;

	protected String predicate;
	@OneToOne(cascade = CascadeType.ALL)
	protected Thing subject;
	protected String data;

	protected EDataType type;

	@OneToOne(cascade = CascadeType.ALL)
	private Info info;

	public DataProperty() {
	}

	@Override
	public String getSource() {
		return info.getSource();
	}

	@Override
	public Date getTime() {
		return info.getTime();
	}

	@Override
	public void setSource(String source) {
		info.setSource(source);
	}

	@Override
	public void setTime(Date time) {
		info.setTime(time);
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public int getId() {
		return id;
	}

	private static int getNewId(Session session) {
		Integer pId = (Integer) session.createSQLQuery("SELECT max(id)+1 FROM dataproperty").uniqueResult();
		if (pId == null)
			pId = 1;
		return pId;
	}

	/* TODO fix it */
	public static String getDataValue(Integer subject, String predicate, Session session) {
		Query q = session.createSQLQuery("SELECT data FROM " + predicate + " WHERE subjectid=:subject");
		q.setInteger("subject", subject);
		return (String) q.uniqueResult();
	}

	public static int getSubjectId(String data, String predicate, Session session){
		Query q = session.createSQLQuery("SELECT subjectid FROM " + predicate + " WHERE data=:data");
		q.setString("data", data);
		return (Integer) q.uniqueResult();
	}
	
	public static int addDataPropertyToSession(Integer subject, String predicate, Object data, EDataType type,
			Session session) {
		return createDataProperty(subject, getNewId(session), predicate, data, type, session);
	}

	public static int updateDataValueOneToOne(Integer subject, String predicate, String newValue, Session session) {
		Query q = session.createQuery("SELECT id FROM " + predicate + " WHERE subjectid = :subject");
		q.setInteger("subject", subject);

		Integer pId = (Integer) q.uniqueResult();

		q = session.createQuery("UPDATE DataProperty SET data=:newValue WHERE id=:id");
		q.setInteger("id", pId);
		q.setString("newValue", newValue);

		q = session.createQuery("UPDATE " + predicate + " SET data=:newValue WHERE id=:id");
		q.setInteger("id", pId);
		q.setString("newValue", newValue);

		return q.executeUpdate();
	}

	public static int updateDataValueOneToOne(Integer subject, String predicate, String oldValue, String newValue,
			Session session) {
		Query q = session.createQuery("SELECT id FROM " + predicate + " WHERE subjectid = :subject");
		q.setInteger("subject", subject);

		Integer pId = (Integer) q.uniqueResult();

		q = session.createSQLQuery("UPDATE DataProperty SET data=:newValue WHERE data=:oldValue AND id=:id");
		q.setInteger("id", pId);
		q.setString("oldValue", oldValue);
		q.setString("newValue", newValue);

		q = session.createSQLQuery("UPDATE " + predicate + " SET data=:newValue WHERE data=:oldValue AND id=:id");
		q.setInteger("id", pId);
		q.setString("oldValue", oldValue);
		q.setString("newValue", newValue);

		return q.executeUpdate();
	}

	public static int createDataProperty(Integer subject, Integer pId, String predicate, Object data, EDataType type,
			Session session) {
		SQLQuery q = session.createSQLQuery("INSERT INTO dataproperty"
				+ " (id, predicate, subject_id, data, type) VALUES (:pId, :predicate, :subject, :data, :type)");
		q.setInteger("pId", pId);
		q.setInteger("subject", subject);
		q.setInteger("type", type.getValue());
		q.setString("predicate", predicate);
		q.setString("data", data.toString());
		q.executeUpdate();

		q = session.createSQLQuery(
				"INSERT INTO " + predicate + " (id, subjectid, data, type) VALUES (:pId, :subject, :data, :type)");
		q.setInteger("pId", pId);
		q.setInteger("subject", subject);
		q.setInteger("type", type.getValue());
		q.setString("data", data.toString());
		return q.executeUpdate();
	}

	/* TODO add value field to data properties too! */
	public static int deleteDataPropertyFromSession(String predicate, String subject, String data, Session session) {
		String select = "SELECT id FROM " + predicate + " WHERE subjectid=:subject AND data=:data";
		
		String delete = "DELETE FROM dataproperty WHERE id=:id";

		Integer subjectId = Thing.getIndividualId(subject, session);

		Query q = session.createSQLQuery(select);
		q.setInteger("subject", subjectId);
		q.setString("data", data);

		Integer id = (Integer) q.uniqueResult();
		q = session.createSQLQuery(delete);
		q.setInteger("id", id);

		return q.executeUpdate();
	}
}

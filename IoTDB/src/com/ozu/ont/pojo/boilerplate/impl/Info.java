package com.ozu.ont.pojo.boilerplate.impl;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.ozu.ont.pojo.boilerplate.interfaces.HasInfo;

@Entity
public class Info implements HasInfo{
	@Id
	@GeneratedValue
	protected int id;
	protected String source;
	protected Date time;
	
	
	public int getId() {
		return id;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
}

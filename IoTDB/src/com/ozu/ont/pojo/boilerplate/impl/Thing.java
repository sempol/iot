package com.ozu.ont.pojo.boilerplate.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.interfaces.IThing;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Thing implements IThing {
	@Id
	@GeneratedValue
	private int id;

	@OneToOne(cascade = CascadeType.ALL)
	private Info info;
	
	@Column(unique = true)
	private String name;

	public Thing() {
	}

	public Thing(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getSource() {
		return info.getSource();
	}

	@Override
	public Date getTime() {
		return info.getTime();
	}

	@Override
	public void setSource(String source) {
		info.setSource(source);
	}

	@Override
	public void setTime(Date time) {
		info.setTime(time);
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}
	
	private static int getNewId(Session session) {
		Integer pId = (Integer) session.createSQLQuery("SELECT max(id)+1 FROM Thing").uniqueResult();
		if (pId == null)
			pId = 1;
		return pId;
	}

	public static Integer getIndividualId(String name, Session session) {
		SQLQuery select = session.createSQLQuery("SELECT id FROM thing WHERE name =:name");
		select.setString("name", name);
		return (Integer) select.uniqueResult();
	}

	public static int createIndividualIfNotExists(String name, Session session) {
		/* TODO test */
		Integer id = getIndividualId(name, session);
		if (id == null) {
			id = getNewId(session);
			Query q = session.createSQLQuery("INSERT INTO Thing (id, name) VALUES (:id, :name)");
			q.setInteger("id", id);
			q.setString("name", name);
			q.executeUpdate();
		}
		return id;
	}

	public static int addIndividualToSession(String name, String type, Session session) {
		Integer id = getNewId(session);
		Query q = session.createSQLQuery("INSERT INTO Thing (id, name) VALUES (:id, :name)");
		q.setInteger("id", id);
		q.setString("name", name);
		q.executeUpdate();
		return Thing.addIndividualToSession(id, type, session);
	}

	public static int addIndividualToSessionIfNotExists(String name, String type, Session session) {
		/* TODO test */
		Integer id = createIndividualIfNotExists(name, session);
		return Thing.addIndividualToSessionIfNotExists(id, type, session);
	}
	
	public static int addIndividualToSessionIfNotExists(int id, String type, Session session) {
		Query q = session.createSQLQuery("SELECT id FROM " + type + " WHERE id=:id");
		q.setInteger("id", id);
		Integer individual = (Integer) q.uniqueResult();
		if (individual == null)
			return addIndividualToSession(id, type, session);
		return individual;
	}

	public static int addIndividualToSession(int id, String type, Session session) {
		Query q = session.createSQLQuery("INSERT INTO " + type + " (id) VALUES (:id)");
		q.setInteger("id", id);
		q.executeUpdate();
		return id;
	}

	public static int renameIndividual(String oldName, String newName, Session session) {
		Query q = session.createQuery("UPDATE Thing SET name=:newName WHERE name=:oldName");
		q.setString("oldName", oldName);
		q.setString("newName", newName);
		return q.executeUpdate();
	}

	public static int deleteIndividualFromSession(int id, Session session) {
		Query q = session.createQuery("DELETE FROM ObjectProperty WHERE subject_id=:id OR object_id=:id");
		q.setInteger("id", id);
		q.executeUpdate();
		
		q = session.createQuery("DELETE FROM DataProperty WHERE subject_id=:id");
		q.setInteger("id", id);
		q.executeUpdate();
		
		q = session.createQuery("DELETE FROM Thing WHERE id=:id");
		q.setInteger("id", id);
		return q.executeUpdate();
	}

	public static int deleteIndividualFromSession(String name, Session session) {
		return deleteIndividualFromSession(getIndividualId(name, session), session);
	}
	
	public static int removeIndividualFromType(String name, String type, Session session) {
		Query q = session.createQuery("DELETE FROM " + type + " WHERE name=:name");
		q.setString("name", name);
		return q.executeUpdate();
	}

	public static List<Integer> getIndividualIds(String className) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			List<Integer> list = session.createSQLQuery("SELECT id FROM " + className).list();
			return list;
		} catch (Exception e) {
			System.err.println(
					"Error occured while generating initial state: GetIndividuals: " + className + e.getMessage());
		} finally {
			session.close();
		}
		/* Error? */
		return null;
	}
	
	public static String getIndividualName(Integer id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			Query q = session.createSQLQuery("SELECT name FROM Thing WHERE thing.id=:id");
			q.setInteger("id", id);
			return (String) q.uniqueResult();
		} catch (Exception e) {
			System.err.println(
					"Error occured while generating initial state: GetIndividualName: " + id + e.getMessage());
		} finally {
			session.close();
		}
		/* Error? */
		return null;
	}
	
	public static List<String> getIndividualFromThing() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			List<String> list = session.createSQLQuery("SELECT name FROM Thing").list();
			return list;
		} catch (Exception e) {
			System.err.println(
					"Error occured while generating initial state: getIndividualFromThing: " + e.getMessage());
		} finally {
			session.close();
		}
		/* Error? */
		return null;
	}
	
	public static List<String> getIndividualNames(String className) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			List<String> list = session.createSQLQuery("SELECT name FROM Thing, " + className + " WHERE thing.id = " + className + ".id").list();
			return list;
		} catch (Exception e) {
			System.err.println(
					"Error occured while generating initial state: GetIndividuals: " + className + e.getMessage());
		} finally {
			session.close();
		}
		/* Error? */
		return null;
	}
}

package com.ozu.ont.pojo.boilerplate.interfaces;

import java.util.Date;

public interface IObjectProperty extends HasInfo{
	
	public String getPredicate();
	public IThing getSubject();
	public IThing getObject();
	
	public int getId();
	public String getSource();
	public Date getTime();
	public void setSource(String source);
	public void setTime(Date time);
	
}

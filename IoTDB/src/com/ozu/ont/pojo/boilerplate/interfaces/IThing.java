package com.ozu.ont.pojo.boilerplate.interfaces;

import java.util.Date;
import java.util.Set;

import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;

public interface IThing extends HasInfo{

	public String getName();
	
	public void setName(String name);

	public int getId();
	
	public String getSource();
	public Date getTime();
	public void setSource(String source);
	public void setTime(Date time);
}

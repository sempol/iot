package com.ozu.ont.pojo.boilerplate.sample;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import com.ozu.ont.pojo.boilerplate.interfaces.IThing;

public class HasAge extends DataProperty{

	
	protected String predicate;
    @OneToOne(cascade = CascadeType.ALL)
    protected Thing subject;
    protected String data;
    protected EDataType type;

    public HasAge(String predicate, IThing subject, String object) {
        this.predicate = predicate;
        this.subject = (Thing) subject;
        this.data = object;
    }

    public String getPredicate() {
        return predicate;
    }

    public IThing getSubject() {
        return subject;
    }

	public String getData() {
		return data;
	}

	@Enumerated(value=EnumType.STRING)
	public EDataType getType() {
		return type;
	}

}

package JSHOP2;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by xx on 01/04/15.
 */
public class JSHOP2QLInterface {
	private static final long serialVersionUID = 112832007;

	// The list of plan steps dynamically set by JSHOP2
	protected static ArrayList<PlanStepInfo> planStepList;

	// The number of plans found in the current problem. This is
	// dynamically set by JSHOP2
	protected static int numPlans;

	// -----------------------------------------------------------------------------

	// Used to iterate through 'planStepList'
	protected int iterator;

	// Used in various places when a global counter is convenient
	protected int count;

	// Tracks which plan is currently being worked on
	protected int planNumber;

	// Holds the data that make up a command from the input
	protected PlanStepInfo newCommand;

	// A string that temporarily holds the method info that will be
	// transferred to the "reduced" command that will follow
	protected String newMethod;

	// A vector to store the leaf nodes that make up the plan. The elements are
	// DefaultMutableTreeNode's, and their indices represent the order in which
	// they were
	// visited.
	protected ArrayList<DefaultMutableTreeNode> leafNodes;

	// The tree model that contains all the data within the tree structure
	protected DefaultTreeModel treeModel;

	// The invisible root node that is the parent for the first node displayed
	// in the tree
	protected DefaultMutableTreeNode rootNode;

	// A hashtable of DefaultMutableTreeNode references used to keep track of
	// the nodes in the tree.
	// Currently, nodes that are deleted in the tree when backtracking aren't
	// deleted in this
	// hashtable. This doesn't seem to cause a problem except that it may become
	// inefficient for
	// large domains that backtrack a lot. In those cases, a search for a
	// particular node within
	// treeNodeReferences may waste a lot of time looking over nodes that have
	// been deleted.
	protected Hashtable<Integer, DefaultMutableTreeNode> treeNodeReferences;

	// The name of the currently selected node
	protected String selectedNodeName;

	public JSHOP2QLInterface() {
		initFieldsAndCreateInterface();
	}

	// -----------------------------------------------------------------------------
	/**
	 * The default constructor. Call this constructor only after the plan step
	 * list has been initialized by <code>setPlanStepList</code> and the the
	 * number of plans has been set by <code>setNumPlans</code>. When called
	 * successfully, this constructor will launch the GUI.
	 *
	 */
	// -----------------------------------------------------------------------------

	/**
	 * This function is used to pass in the list of plan steps that represent
	 * the actions taken by JSHOP2 to find plans for the current problem.
	 *
	 * @param inputList
	 *            - an ArrayList of PlanStepInfo objects that are used to
	 *            reconstruct the plan finding process
	 */
	public static void setPlanStepList(ArrayList<PlanStepInfo> inputList) {
		planStepList = inputList;
	}

	/**
	 * This function is used to set the total number of plans found for the
	 * current problem by JSHOP2
	 *
	 * @param numPlansIn
	 *            - an integer representing the total number of plans found
	 */
	public static void setNumPlans(int numPlansIn) {
		numPlans = numPlansIn;
	}

	// -----------------------------------------------------------------------------

	/**
	 * Initializes all fields and constructs the graphical interface
	 */
	private void initFieldsAndCreateInterface() {
		iterator = 0;
		count = 0;
		planNumber = 0;
		treeNodeReferences = new Hashtable<Integer, DefaultMutableTreeNode>();

		newMethod = "";
		selectedNodeName = "";

		// createPlanList();
		leafNodes = new ArrayList<DefaultMutableTreeNode>();

		// ********************************************************* //
		// ------------------ Creating the Center ------------------ //
		// ********************************************************* //

		// Creating tree and its scroll pane
		rootNode = new DefaultMutableTreeNode("ROOTNODE");
		treeModel = new DefaultTreeModel(rootNode);
	}

	// -----------------------------------------------------------------------------

	/**
	 * Executes a single step in the plan step list
	 */
	public boolean runAllSteps() {
		boolean retval = true;
		ArrayList<Node> toAdd = new ArrayList<Node>();
		DefaultMutableTreeNode parent = null;

		while (iterator < planStepList.size()) {
			PlanStepInfo step = planStepList.get(iterator++);

			// if a plan has been found
			if (step.planFound == true) {
				processPlanFound();

				// retval is set to false here so that the "run" feature will
				// stop every time
				// a plan is found.
				retval = false;
			}

			// trying a task
			else if (step.action.equals("TRYING")) {
				// If the last step was a "plan found" step, then the leafNodes
				// vector has to be cleared now that it is working on a new
				// plan. Here,
				// iterator is subtracted by 2 due to the fact that it's been
				// post-incremented
				// above. This step assumes that a "plan found" step will always
				// be followed
				// by a "trying" step, and it's implemented to enable dynamic
				// leaf node tracking.
				if (iterator - 2 >= 0)
					if (planStepList.get(iterator - 2).planFound == true)
						leafNodes.clear();
				parent = processTrying(step, toAdd);
			}

			// reducing a task
			else if (step.action.equals("REDUCED"))
				parent = processReduced(step, toAdd);

			else if (step.action.equals("STATECHANGED"))
				parent = processStateChanged(step);

			else if (step.action.equals("SETGOALTASKS"))
				parent = processSetGoalTasks(step, toAdd);

			// backtracking
			else if (step.action.equals("BACKTRACKING"))
				processBacktracking(step);

			// adding nodes to treeModel and treeNodeReferences
			for (int i = 0; i < toAdd.size(); i++) {
				Node add = toAdd.get(i);
				DefaultMutableTreeNode child = new DefaultMutableTreeNode(add);
				treeNodeReferences.put(add.ID, child);

				treeModel.insertNodeInto(child, parent, parent.getChildCount());
			}

		}

		return retval;
	}

	// -----------------------------------------------------------------------------
	// -----------------------------------------------------------------------------

	/**
	 * Helper function to runOneStep(). This function executes the steps
	 * required to display the plan on-screen
	 */
	private void processPlanFound() {
		planNumber++;
		ArrayList<String> plan = new ArrayList<String>(); // vector of strings
															// containing the
															// leaves' numbered
															// names

		// Labeling leaf nodes with appropriate numbers.
		// Although renumberLeaves() does the same thing, it isn't called here
		// so that planList can be created in the process of renumbering the
		// leaves.
		for (int i = 0; i < leafNodes.size(); i++) {
			DefaultMutableTreeNode leaf = leafNodes.get(i);
			Node node = (Node) leaf.getUserObject();
			node.tag = i + 1;
			plan.add(node.toString());

			treeModel.nodeChanged(leaf);
		}

		// Creating found plan dialog box
		/*
		 * String title = "Plan "; title += String.valueOf(planNumber); title +=
		 * " of "; title += String.valueOf(numPlans);
		 */
		if (planNumber == 1) {
			File file = new File("plan-details.out");

			PrintWriter writer = null;
			try {
				if (!file.exists())
					file.createNewFile();

				writer = new PrintWriter(file);
				writer.print("(");
				writer.close();
			} catch (FileNotFoundException e) {
				System.err.println("Could not locate plan-details.out");
			} catch (IOException e) {
				System.err.println("Could not create plan-details.out");
			}
		}

		System.out.println("PlanNumber = " + planNumber + " Number of Plans: " + numPlans);
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("plan-details.out", true)))) {
			String title = "(:plan-" + String.valueOf(planNumber) + " ";
			out.println(title);

			/*
			 * TODO check if there is a bug while writing the add list. It skips
			 * the first statement?
			 */
			for (String s : plan)
				out.println(s);

			out.println(")");

			if (planNumber == numPlans) {
				out.println(")");
			}

		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}

		// System.out.println(plan);
		// new PlanDialog( title, plan );
	}

	// -----------------------------------------------------------------------------

	/**
	 * Helper function to runOneStep(). This function determines the current
	 * state of the world for every node and inserts the goal task into the
	 * tree.
	 */
	private DefaultMutableTreeNode processTrying(PlanStepInfo step, ArrayList<Node> toAdd) {
		DefaultMutableTreeNode parent = null;

		// if adding the root node
		if (iterator == 1) {
			parent = rootNode;
			Node temp = new Node(step.taskAtom.toString(), step.taskAtom.getHead().getID());
			temp.state = step.state;
			toAdd.add(temp);

			// I should add temp to the leafNodes list here, but it's
			// problematic
			// because I don't have access to its DefaultMutableTreeNode wrapper
			// object
			// due to it not being created yet. This is only a problem in the
			// trivial case
			// where the plan consists of only the root node, so I deemed it
			// unnecessary to
			// deal with...for now.

		} else { // setting node's state to current state
			DefaultMutableTreeNode treeNode = treeNodeReferences.get(step.taskAtom.getHead().getID());
			Node temp = (Node) treeNode.getUserObject();
			temp.state = step.state;
			treeModel.nodeChanged(treeNode);

			// adding the node as a leaf, as long as it's not (!!INOP)
			if (!step.taskAtom.equals("(!!INOP)")) {
				leafNodes.add(treeNode);
				renumberLeaves();
			}

			parent = treeNode;
		}

		return parent;
	}

	// -----------------------------------------------------------------------------

	/**
	 * Helper function to runOneStep(). This function adds children to existing
	 * nodes and marks them if they are ordered.
	 */
	private DefaultMutableTreeNode processReduced(PlanStepInfo step, ArrayList<Node> toAdd) {
		DefaultMutableTreeNode parent = null;

		// The term "parent" here is referring to the current node (parent as in
		// parent of
		// the children that are about to be added).
		parent = treeNodeReferences.get(step.taskAtom.getHead().getID());

		// Setting the method name for this node
		Node node = (Node) parent.getUserObject();
		node.method = step.method;

		// Removing this node from the leaf nodes list, since it now has
		// children.
		// This step assumes that a REDUCED statement will always follow a
		// TRYING
		// statement pertaining to the same task atom, resulting in the deletion
		// of the
		// last element in leafNodes.
		if (!leafNodes.isEmpty()) {
			// removing numbering from this node
			node.tag = null;
			treeModel.nodeChanged(parent);

			// removing this node from leafNodes list
			leafNodes.remove(leafNodes.size() - 1);
		}

		// backtrack if this node has already been reduced and is being reduced
		// again
		if (!parent.isLeaf())
			backtrack(parent);

		// creating the children to be added
		for (int i = 0; i < step.children.length; i++) {
			String childName = step.children[i].getTask().toString();
			Node newNode = new Node(childName, step.children[i].getTask().getHead().getID());
			if (!step.ordered)
				newNode.ordered = false;
			toAdd.add(newNode);
		}

		return parent;
	}

	// -----------------------------------------------------------------------------

	private DefaultMutableTreeNode processStateChanged(PlanStepInfo step) {
		DefaultMutableTreeNode retval = null;

		retval = treeNodeReferences.get(step.taskAtom.getHead().getID());

		// Setting delAdd for this node
		Node node = (Node) retval.getUserObject();
		node.delAdd = step.delAdd;

		// Changing name to intantiated version
		node.name = step.operatorInstance;

		return retval;
	}

	// -----------------------------------------------------------------------------

	/**
	 * Sets the goal tasks for the problem. The goal tasks are stored in the
	 * 'children' field of the 'step' parameter. Each element of 'children'
	 * represents a TaskList that encapsulates a goal task. Calling getTask() on
	 * each TaskList gives access to the TaskAtom that represents the goal task.
	 * If getTask() returns null, then that means the goal task is encapsulated
	 * one or more levels down and getSubtasks() must be called. This is done
	 * recursively by 'setGoalTasksHelper'.
	 */
	private DefaultMutableTreeNode processSetGoalTasks(PlanStepInfo step, ArrayList<Node> toAdd) {
		DefaultMutableTreeNode retval = null;
		retval = rootNode;
		setGoalTasksHelper(step.children, step, toAdd);
		return retval;
	}

	private void setGoalTasksHelper(TaskList[] children, PlanStepInfo step, ArrayList<Node> toAdd) {
		for (int i = 0; i < children.length; i++) {
			TaskList child = children[i];
			if (child.getTask() != null) {
				Node temp = new Node(child.getTask().toString(), child.getTask().getHead().getID());
				temp.state = step.state;
				temp.ordered = step.ordered;
				toAdd.add(temp);
			} else if (child.getSubtasks() != null)
				setGoalTasksHelper(child.getSubtasks(), step, toAdd);
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * Helper function to runOneStep(). This function takes care of backtracking
	 * procedures.
	 */
	private void processBacktracking(PlanStepInfo step) {
		DefaultMutableTreeNode treeNode = treeNodeReferences.get(step.taskAtom.getHead().getID());

		if (treeNode == null)
			return;

		if (!treeNode.isLeaf())
			backtrack(treeNode);

		// If backtracking from a leaf node, check to see if this leaf node was
		// the most recent
		// leaf node to be added to 'leafNodes'. If true, then delete this leaf
		// node's tag and
		// remove it from 'leafNodes'.
		// <Note:> I'm assuming that whenever you backtrack from a leaf node,
		// that leaf node will
		// always have the highest tag value in 'leafNodes'. I'm not certain if
		// this is always the case.
		else {
			if (leafNodes.size() == 0)
				return;

			Node treeNodeUserObj = (Node) treeNode.getUserObject();
			Node leafNodeUserObj = (Node) leafNodes.get(leafNodes.size() - 1).getUserObject();

			if (treeNodeUserObj.ID == leafNodeUserObj.ID) {
				// removing numbering from this node
				treeNodeUserObj.tag = null;
				treeModel.nodeChanged(treeNode);

				// removing this node from leafNodes list
				leafNodes.remove(leafNodes.size() - 1);
			}
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * This function handles the procedures that take place when backtracking.
	 * First, it modifies the leafNodes list and then deletes any children under
	 * treeNode.
	 */
	private void backtrack(DefaultMutableTreeNode treeNode) {
		// removing any leaves in leafNodes that are descendants of treeNode
		for (int i = 0; i < leafNodes.size(); i++) {
			if (treeNode.isNodeDescendant(leafNodes.get(i))) {
				leafNodes.remove(i);
				i--;
			}
		}

		// reunumbering leaves on-screen
		renumberLeaves();

		// deleting children from this node
		treeNode.removeAllChildren();

		treeModel.reload(treeNode);
	}

	// -----------------------------------------------------------------------------

	/**
	 * This function renumbers all current leaf nodes in the order they were
	 * visited relative to one another.
	 */
	private void renumberLeaves() {
		for (int i = 0; i < leafNodes.size(); i++) {
			DefaultMutableTreeNode leaf = leafNodes.get(i);
			Node node = (Node) leaf.getUserObject();
			node.tag = i + 1;

			treeModel.nodeChanged(leaf);
		}
	}

	// -----------------------------------------------------------------------------

	/**
	 * User object utilized by the tree model
	 */
	private class Node {
		public String name;
		public String method;
		public boolean ordered;
		public ArrayList<String> state;
		public Integer tag;
		public int ID;
		public Vector[] delAdd;

		public Node(String nameIn, int IDin) {
			name = nameIn;
			method = "";
			ordered = true;
			state = null;
			tag = null;
			ID = IDin;
			delAdd = null;
		}

		public String toString() {
			if (tag == null)
				return name;
			else {
				/*
				 * String newName = "[ "; newName += String.valueOf(
				 * tag.intValue() ); newName += " ]    ";
				 */
				String newName = "(:operator " + name;
				newName += "\n";

				for (int i = 0; i < delAdd.length; i++) {
					if (delAdd[i].size() > 0) {
						if (i == 0)
							newName += "(delete ";

						if (i == 1)
							newName += "(add ";
					}

					for (int j = 0; j < delAdd[i].size(); j++) {
						newName += delAdd[i].get(j).toString();
					}

					if (delAdd[i].size() > 0 && i <= 1)
						newName += ")";
				}

				newName += ")";

				return newName;
			}
		}

		public boolean hasState() {
			return state != null;
		}

		public String getState() {
			String retval = null;

			// the state is unknown
			if (state == null)
				retval = "Unknown";
			// the state is empty
			else if (state.size() == 0) {
				retval = "";
			} else {
				retval = state.get(0);
				for (int i = 1; i < state.size(); i++) {
					retval += "\n";
					retval += state.get(i);
				}
			}
			return retval;
		}

		public String getStateSize() {
			String retval = null;

			// if the state is unknown
			if (state == null)
				retval = "--";

			// if the state is known
			else
				retval = String.valueOf(state.size());

			return retval;
		}
	}

	// -----------------------------------------------------------------------------

}

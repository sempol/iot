(define 
  (problem iot) 
  (:domain iot) 
  (:objects device0 device1 mother someoneAtFrontDoor PlaySoundSpeaker SendSMS DisplayMessageOnTv) 
  (:init 
    (Person mother) 
    (Event someoneAtFrontDoor) 
    (SoundAction PlaySoundSpeaker) 
    (SMSAction SendSMS) 
    (VisualAction DisplayMessageOnTv) 
    (= 
    (total-cost) 0) 
    (canPerform device0 PlaySoundSpeaker) 
    (= 
      (p1Cost device0) 4) 
    (canPerform device0 DisplayMessageOnTv) 
    (= 
      (p2Cost device0) 0) 
    (canPerform device1 PlaySoundSpeaker) 
    (= 
      (p1Cost device1) 0) 
    (canPerform device1 SendSMS) 
    (= 
      (p3Cost device1) 0)) 
  (:goal 
    (and 
      (gotNotifiedFor mother someoneAtFrontDoor))) 
  (:metric minimize 
    (total-cost)))

(define 
  (problem iot) 
  (:domain iot) 
  (:objects device0 device1 device2 device3 device4 device5 device6 device7 device8 device9 device10 device11 device12 device13 device14 device15 device16 device17 device18 device19 mother someoneAtFrontDoor PlaySoundSpeaker SendSMS DisplayMessageOnTv) 
  (:init 
    (Person mother) 
    (Event someoneAtFrontDoor) 
    (SoundAction PlaySoundSpeaker) 
    (SMSAction SendSMS) 
    (VisualAction DisplayMessageOnTv) 
    (= 
    (total-cost) 0) 
    (canPerform device0 PlaySoundSpeaker) 
    (= 
      (p1Cost device0) 4) 
    (canPerform device0 DisplayMessageOnTv) 
    (= 
      (p2Cost device0) 0) 
    (canPerform device1 SendSMS) 
    (= 
      (p3Cost device1) 0) 
    (canPerform device2 PlaySoundSpeaker) 
    (= 
      (p1Cost device2) 0) 
    (canPerform device3 SendSMS) 
    (= 
      (p3Cost device3) 0) 
    (canPerform device4 PlaySoundSpeaker) 
    (= 
      (p1Cost device4) 0) 
    (canPerform device4 DisplayMessageOnTv) 
    (= 
      (p2Cost device4) 0) 
    (canPerform device5 DisplayMessageOnTv) 
    (= 
      (p2Cost device5) 1) 
    (canPerform device6 PlaySoundSpeaker) 
    (= 
      (p1Cost device6) 4) 
    (canPerform device7 PlaySoundSpeaker) 
    (= 
      (p1Cost device7) 0) 
    (canPerform device7 DisplayMessageOnTv) 
    (= 
      (p2Cost device7) 0) 
    (canPerform device8 SendSMS) 
    (= 
      (p3Cost device8) 0) 
    (canPerform device9 DisplayMessageOnTv) 
    (= 
      (p2Cost device9) 0) 
    (canPerform device10 SendSMS) 
    (= 
      (p3Cost device10) 0) 
    (canPerform device11 PlaySoundSpeaker) 
    (= 
      (p1Cost device11) 0) 
    (canPerform device11 SendSMS) 
    (= 
      (p3Cost device11) 0) 
    (canPerform device12 PlaySoundSpeaker) 
    (= 
      (p1Cost device12) 0) 
    (canPerform device12 DisplayMessageOnTv) 
    (= 
      (p2Cost device12) 0) 
    (canPerform device13 PlaySoundSpeaker) 
    (= 
      (p1Cost device13) 0) 
    (canPerform device14 PlaySoundSpeaker) 
    (= 
      (p1Cost device14) 0) 
    (canPerform device14 SendSMS) 
    (= 
      (p3Cost device14) 0) 
    (canPerform device15 SendSMS) 
    (= 
      (p3Cost device15) 0) 
    (canPerform device16 PlaySoundSpeaker) 
    (= 
      (p1Cost device16) 0) 
    (canPerform device17 DisplayMessageOnTv) 
    (= 
      (p2Cost device17) 3) 
    (canPerform device18 DisplayMessageOnTv) 
    (= 
      (p2Cost device18) 0) 
    (canPerform device18 SendSMS) 
    (= 
      (p3Cost device18) 3) 
    (canPerform device19 DisplayMessageOnTv) 
    (= 
      (p2Cost device19) 0)) 
  (:goal 
    (and 
      (gotNotifiedFor mother someoneAtFrontDoor))) 
  (:metric minimize 
    (total-cost)))

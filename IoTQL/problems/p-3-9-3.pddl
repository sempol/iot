(define 
  (problem iot) 
  (:domain iot) 
  (:objects device0 device1 device2 mother someoneAtFrontDoor PlaySoundSpeaker SendSMS DisplayMessageOnTv) 
  (:init 
    (Person mother) 
    (Event someoneAtFrontDoor) 
    (SoundAction PlaySoundSpeaker) 
    (SMSAction SendSMS) 
    (VisualAction DisplayMessageOnTv) 
    (= 
    (total-cost) 0) 
    (canPerform device0 PlaySoundSpeaker) 
    (= 
      (p1Cost device0) 3) 
    (canPerform device1 SendSMS) 
    (= 
      (p3Cost device1) 8) 
    (canPerform device2 SendSMS) 
    (= 
      (p3Cost device2) 4)) 
  (:goal 
    (and 
      (gotNotifiedFor mother someoneAtFrontDoor))) 
  (:metric minimize 
    (total-cost)))

(define 
  (problem iot) 
  (:domain iot) 
  (:objects device0 device1 device2 device3 device4 mother someoneAtFrontDoor PlaySoundSpeaker SendSMS DisplayMessageOnTv) 
  (:init 
    (Person mother) 
    (Event someoneAtFrontDoor) 
    (SoundAction PlaySoundSpeaker) 
    (SMSAction SendSMS) 
    (VisualAction DisplayMessageOnTv) 
    (= 
    (total-cost) 0) 
    (canPerform device0 PlaySoundSpeaker) 
    (= 
      (p1Cost device0) 0) 
    (canPerform device0 DisplayMessageOnTv) 
    (= 
      (p2Cost device0) 0) 
    (canPerform device0 SendSMS) 
    (= 
      (p3Cost device0) 0) 
    (canPerform device1 SendSMS) 
    (= 
      (p3Cost device1) 0) 
    (canPerform device2 DisplayMessageOnTv) 
    (= 
      (p2Cost device2) 1) 
    (canPerform device3 PlaySoundSpeaker) 
    (= 
      (p1Cost device3) 0) 
    (canPerform device3 SendSMS) 
    (= 
      (p3Cost device3) 0) 
    (canPerform device4 PlaySoundSpeaker) 
    (= 
      (p1Cost device4) 0) 
    (canPerform device4 SendSMS) 
    (= 
      (p3Cost device4) 0)) 
  (:goal 
    (and 
      (gotNotifiedFor mother someoneAtFrontDoor))) 
  (:metric minimize 
    (total-cost)))

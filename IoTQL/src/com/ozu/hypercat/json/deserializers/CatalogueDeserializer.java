package com.ozu.hypercat.json.deserializers;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.ozu.hypercat.objects.HypercatCatalogue;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.HypercatTriple;

public class CatalogueDeserializer implements JsonDeserializer<HypercatCatalogue>{

	@Override
	public HypercatCatalogue deserialize(JsonElement json, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {

	    final JsonObject jsonObject = json.getAsJsonObject();
	    final HypercatCatalogue cat = new HypercatCatalogue();
	    final Gson gson = new Gson();
	    
	    Type itemList = new TypeToken<List<HypercatItem>>() {}.getType();
	    cat.setItems(gson.fromJson(jsonObject.get("items").getAsJsonArray(), itemList));
	    
	    Type metaList = new TypeToken<HashSet<HypercatTriple>>() {}.getType();
	    cat.setMetadata(gson.fromJson(jsonObject.get("item-metadata").getAsJsonArray(), metaList));
	    
	    if (cat.isValid())
	    	return cat;
	    else
	    	return null;
	    }
}

package com.ozu.hypercat.objects;

import static com.ozu.hypercat.objects.Rel.HAS_DESCRIPTION;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class HypercatItem extends HypercatObject {
	@Expose
	@SerializedName("i-object-metadata")
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable
	protected Set<HypercatTriple> metadata = new HashSet<HypercatTriple>();

	public HypercatItem() {
	}

	public HypercatItem(String href, String description) {
		this.href = href;
		addDescription(description);
	}

	@Override
	public Set<HypercatTriple> getMetadata() {
		return metadata;
	}

	public void setMetadata(Set<HypercatTriple> metadata) {
		this.metadata = metadata;
	}

	@Override
	public boolean isValid() {
		boolean validDescription = false;
		boolean validHref = false;

		for (HypercatTriple data : metadata) {
			if (data.getRel().equals(HAS_DESCRIPTION))
				validDescription = true;
		}

		if (href != null || href.length() > 0)
			validHref = true;

		return validDescription & validHref;
	}

	@Override
	public String toString() {
		return "HypercatItem [metadata=" + metadata + ", href=" + href + "]";
	}

}

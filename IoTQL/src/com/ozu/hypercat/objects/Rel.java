package com.ozu.hypercat.objects;


public class Rel {

	public final static String CAT = "cats";
	
	public final static String CONTENT_TYPE = "urn:X-tsbiot:rels:isContentType";
	public final static String CATALOGUE_JSON= "application/vnd.tsbiot.catalogue+json";
	public final static String SUPPORTS_SEARCH = "urn:X-tsbiot:rels:supportsSearch";
	public final static String SIMPLE_SEARCH = "urn:X-tsbiot:search:simple";

	public final static String HAS_DESCRIPTION = "urn:X-tsbiot:rels:hasDescription:en";
	
}

package com.ozu.hypercat.objects;

import com.google.gson.annotations.Expose;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;

import static com.ozu.utils.StringConstants.HAS_BOOLEAN_VALUE;
import static com.ozu.utils.StringConstants.HAS_DOUBLE_VALUE;
import static com.ozu.utils.StringConstants.HAS_STRING_VALUE;

public class SenmlParameter {
	@Expose
	// Name
	private String n;
	@Expose
	// Units
	private String u;
	@Expose
	// String Value
	private String sv;
	@Expose
	// Value
	private Double v;
	@Expose
	// Value Sum
	private Double s;
	@Expose
	// Boolean Value
	private Boolean bv;
	@Expose
	// Time
	private Number t;
	@Expose
	// Update Time
	private Number ut;

	public Double getValueSum() {
		return s;
	}

	public String getName() {
		return n;
	}

	public String getUnit() {
		return u;
	}

	public String getStringValue() {
		return sv;
	}

	public Double getValue() {
		return v;
	}

	public Boolean getBooleanValue() {
		return bv;
	}

	public Number getTime() {
		return t;
	}

	public Number getUpdateTime() {
		return ut;
	}
	
	public String getPredicate() {
		if (bv != null)
			return HAS_BOOLEAN_VALUE;
		if (sv != null)
			return HAS_STRING_VALUE;
		return HAS_DOUBLE_VALUE;
	}
	
	public String getValueAsString() {
		if (s != null)
			return s.toString();
		if (sv != null)
			return sv;
		if (v != null)
			return v.toString();
		if (bv != null)
			return bv.toString();
		return null;
	}

	public EDataType getValueType() {
		if (bv != null)
			return EDataType.BOOLEAN;
		if (sv != null)
			return EDataType.STRING;
		return EDataType.DOUBLE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bv == null) ? 0 : bv.hashCode());
		result = prime * result + ((n == null) ? 0 : n.hashCode());
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		result = prime * result + ((sv == null) ? 0 : sv.hashCode());
		result = prime * result + ((t == null) ? 0 : t.toString().hashCode());
		result = prime * result + ((u == null) ? 0 : u.hashCode());
		result = prime * result + ((ut == null) ? 0 : ut.toString().hashCode());
		result = prime * result + ((v == null) ? 0 : v.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SenmlParameter other = (SenmlParameter) obj;
		if (bv == null) {
			if (other.bv != null)
				return false;
		} else if (!bv.equals(other.bv))
			return false;
		if (n == null) {
			if (other.n != null)
				return false;
		} else if (!n.equals(other.n))
			return false;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		if (sv == null) {
			if (other.sv != null)
				return false;
		} else if (!sv.equals(other.sv))
			return false;
		if (t == null) {
			if (other.t != null)
				return false;
		} else if (t.longValue() != other.t.longValue())
			return false;
		if (u == null) {
			if (other.u != null)
				return false;
		} else if (!u.equals(other.u))
			return false;
		if (ut == null) {
			if (other.ut != null)
				return false;
		} else if (ut.longValue() != other.ut.longValue())
			return false;
		if (v == null) {
			if (other.v != null)
				return false;
		} else if (!v.equals(other.v))
			return false;
		return true;
	}

	public Boolean isValid() {
		int cnt = 0;

		if (v != null)
			cnt++;
		if (s != null)
			cnt++;
		if (bv != null)
			cnt++;

		if (s == null && cnt == 1)
			return true;
		else if (s != null && cnt == 0)
			return true;
		
		return false;
	}

	@Override
	public String toString() {
		return "SenmlParameter [n=" + n + ", u=" + u + ", sv=" + sv + ", v="
				+ v + ", s=" + s + ", bv=" + bv + ", t=" + t + ", ut=" + ut
				+ "]";
	}
}

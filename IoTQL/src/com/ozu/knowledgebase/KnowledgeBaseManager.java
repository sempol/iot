package com.ozu.knowledgebase;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.Session;
import org.xml.sax.SAXException;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.knowledgebase.devices.DeviceObligationObserver;
import com.ozu.knowledgebase.devices.DevicePollerMaster;
import com.ozu.knowledgebase.hypercat.commands.HypercatCommand;
import com.ozu.knowledgebase.hypercat.commands.SenmlCommand;
import com.ozu.owlql.InconsistentKBException;
import com.ozu.owlql.QLReasoner;
import com.ozu.owlql.ReasonerFactory;
import com.ozu.policy.PolicyManager;
import com.ozu.policy.PolicyReasoner;

/**
 * Created by xx on 17/05/15.
 */
public class KnowledgeBaseManager {

	private static KnowledgeBaseManager instance = null;
	private QLReasoner reasoner = null;
	private PolicyReasoner policyReasoner = null;
	private DeviceObligationObserver obligationObserver = null;
	private DevicePollerMaster devicePoller = null;

	private KnowledgeBaseManager()
			throws ParserConfigurationException, IOException, SAXException, InconsistentKBException {
		start();
	}

	public void start() throws ParserConfigurationException, IOException, SAXException, InconsistentKBException {
		reasoner = new ReasonerFactory().getReasoner();

		PolicyManager.getInstance().readPolicies();

		policyReasoner = new PolicyReasoner();
		policyReasoner.updateNormativeState();

		/* TODO Make it consistent! */
		if (!reasoner.isConsistent())
			throw new InconsistentKBException("KBManager cannot be started. KB is inconsistent.");

		obligationObserver = policyReasoner.getObligationObserver();
		obligationObserver.start();

		devicePoller = new DevicePollerMaster();
		devicePoller.start();
	}

	public void stop() {
		devicePoller.stopThread();
		obligationObserver.stopThread();
	}

	public static KnowledgeBaseManager getInstance()
			throws ParserConfigurationException, IOException, SAXException, InconsistentKBException {
		if (instance == null) {
			synchronized (KnowledgeBaseManager.class) {
				if (instance == null)
					instance = new KnowledgeBaseManager();
			}
		}
		return instance;
	}

	/* Old Info can be null */
	public void updateFromSenmlFile(SenmlCommand info) {
		System.out.println("Updating - " + info);

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			info.execute(session);

			reasoner.setSession(session);
			if (reasoner.isConsistent()) {
				session.getTransaction().commit();
				policyReasoner.updateNormativeState();
				return;
			}

			// Do something smart
			session.getTransaction().rollback();
		} catch (Exception e) {
			System.err.println(
					"Exception occured while processing information: " + info.toString() + " , " + e.getMessage());
		} finally {
			session.close();
		}
	}

	public void processHypercatCommand(HypercatCommand info) {
		System.out.println("Processing - " + info);

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			info.execute(session);

			reasoner.setSession(session);
			if (reasoner.isConsistent()) {
				session.getTransaction().commit();
				policyReasoner.updateNormativeState();
				return;
			}

			// Do something smart
			session.getTransaction().rollback();
		} catch (Exception e) {
			System.err.println(
					"Exception occured while processing information: " + info.toString() + " , " + e.getMessage());
			session.getTransaction().rollback();
		} finally {
			session.close();
		}
	}
}

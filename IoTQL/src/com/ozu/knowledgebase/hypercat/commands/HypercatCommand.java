package com.ozu.knowledgebase.hypercat.commands;

import static com.ozu.utils.StringConstants.ONTOLOGY_IRI;
import static com.ozu.utils.StringConstants.RDF_SYNTAX_TYPE;

import java.util.HashMap;

import org.hibernate.Session;

import com.ozu.hypercat.objects.HypercatObject;
import com.ozu.hypercat.objects.HypercatTriple;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import static com.ozu.utils.StringConstants.XSD_DOUBLE;
import static com.ozu.utils.StringConstants.XSD_BOOLEAN;

public abstract class HypercatCommand {

	protected HashMap<String, Integer> cache;
	protected HypercatObject info;
	protected Session session;

	public abstract void execute(Session session);

	public abstract void undo();

	protected Integer getOrCreateIndividual(String name) {
		if (cache.containsKey(name))
			return cache.get(name);

		Integer current = Thing.createIndividualIfNotExists(name, session);
		cache.put(name, current);
		return current;
	}

	protected String extractName(HypercatTriple t) {
		String name = t.getVal();
		if (name.contains("#"))
			name = name.substring(name.lastIndexOf("#") + 1);
		return name;
	}

	protected void addTriple(int subject, HypercatTriple t) {
		if (t.getRel().contains(RDF_SYNTAX_TYPE)) {
			String type = t.getVal().substring(t.getVal().lastIndexOf("#") + 1);
			Thing.addIndividualToSessionIfNotExists(subject, type, session);
		} else if (t.getRel().contains(ONTOLOGY_IRI)) {
			String predicate = t.getRel().substring(t.getRel().lastIndexOf("#") + 1);
			if (isDataProperty(t.getVal())) {
				String data = t.getVal();
				addDataProperty(subject, predicate, data);
			} else {
				Integer object = getOrCreateIndividual(t.getVal().substring(t.getVal().lastIndexOf("#") + 1));
				ObjectProperty.addObjectPropertyToSession(predicate, subject, object, session);
			}
		}
	}

	protected void deleteTriple(String subject, HypercatTriple t) {
		if (t.getRel().contains(RDF_SYNTAX_TYPE)) {
			String type = t.getVal().substring(t.getVal().lastIndexOf("#") + 1);
			Thing.removeIndividualFromType(subject, type, session);
		} else if (t.getRel().contains(ONTOLOGY_IRI)) {
			String predicate = t.getRel().substring(t.getRel().lastIndexOf("#") + 1);
			if (isDataProperty(t.getVal())) {
				String data = t.getVal();
				DataProperty.deleteDataPropertyFromSession(predicate, subject, data, session);
			} else {
				String object = t.getVal().substring(t.getVal().lastIndexOf("#") + 1);
				ObjectProperty.deleteObjectPropertyFromSession(predicate, subject, object, session);
			}
		}
	}

	protected void addDataProperty(int subject, String predicate, String data) {
		EDataType type = EDataType.STRING;

		String typeStr = data.substring(data.lastIndexOf("^") + 1);
		if (typeStr.contains(XSD_DOUBLE))
			type = EDataType.DOUBLE;
		else if (typeStr.contains(XSD_BOOLEAN))
			type = EDataType.BOOLEAN;

		data = data.substring(data.indexOf("\"") + 1, data.lastIndexOf("\""));
		DataProperty.addDataPropertyToSession(subject, predicate, data, type, session);
	}

	protected boolean isDataProperty(String value) {
		return value.contains("xsd:");
	}

	@Override
	public abstract String toString();

}

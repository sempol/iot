	package com.ozu.knowledgebase.hypercat.commands;

import static com.ozu.utils.StringConstants.RDF_SYNTAX_ABOUT;

import java.util.HashMap;
import java.util.Iterator;

import org.hibernate.Session;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.ozu.hypercat.objects.HypercatObject;
import com.ozu.hypercat.objects.HypercatTriple;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;

public class UpdateInformation extends HypercatCommand {
	private HypercatObject oldInfo;

	public UpdateInformation(HypercatObject info, HypercatObject oldInfo) {
		this.info = info;
		this.oldInfo = oldInfo;
		this.cache = new HashMap<String, Integer>();
		this.session = null;
	}

	@Override
	public void execute(Session session) {
		this.session = session;
		updateHypercatItem(oldInfo, info);
		this.session = null;
	}

	private void updateHypercatItem(HypercatObject oldObj, HypercatObject newObj) {
		/* TODO can this solution be improved? */
		SetView<HypercatTriple> addSet = Sets.difference(newObj.getMetadata(), oldObj.getMetadata());
		SetView<HypercatTriple> deleteSet = Sets.difference(oldObj.getMetadata(), newObj.getMetadata());
		
		String name = "";
		String newName = "";

		for (HypercatTriple metadata : oldObj.getMetadata()) {
			if (metadata.getRel().contains(RDF_SYNTAX_ABOUT)) {
				name = extractName(metadata);
				break;
			}
		}

		/* TODO raise exception or give error? */
		if (name.length() < 1) {
			System.err.println("Could not find individual name triple " + RDF_SYNTAX_ABOUT);
			return;
		}

		try {
			Integer current = getOrCreateIndividual(name);

			for (Iterator<HypercatTriple> it = addSet.iterator(); it.hasNext();) {
				HypercatTriple curr = it.next();

				/* tmp fix */
				if (curr == null)
					continue;

				if (curr.getRel().contains(RDF_SYNTAX_ABOUT)) {
					newName = extractName(curr);
					continue;
				}
				addTriple(current, curr);
			}

			/* TODO do not pass with name to avoid duplicate selects? create new helper methods? */
			for (Iterator<HypercatTriple> it = deleteSet.iterator(); it.hasNext();)
				deleteTriple(name, it.next());

			if (newName.length() > 0)
				Thing.renameIndividual(name, newName, session);

			if (oldInfo.getHref() != info.getHref())
				DataProperty.updateDataValueOneToOne(current, "hasHref", oldInfo.getHref(), info.getHref(), session);

		} catch (Exception e) {
			System.err.println("Exception occured while processing new item: " + info);
			e.printStackTrace();
			session.getTransaction().rollback();
			return;
		}
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "Update Information: " + info.toString();
	}

}

package com.ozu.owlql;

public class InconsistentKBException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InconsistentKBException() {
		super();
	}

	public InconsistentKBException(String message) {
		super(message);
	}

	public InconsistentKBException(String message, Throwable cause) {
		super(message, cause);
	}

	public InconsistentKBException(Throwable cause) {
		super(cause);
	}
}

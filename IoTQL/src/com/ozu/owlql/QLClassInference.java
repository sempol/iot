package com.ozu.owlql;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.xml.sax.SAXException;

import com.google.common.reflect.TypeToken;
import com.google.gson.GsonBuilder;
import com.hp.hpl.jena.query.Query;
import com.ibm.research.owlql.ConjunctiveQuery;
import com.ibm.research.owlql.OWLQLCompiler;
import com.ozu.policy.PolicyManager;
import com.ozu.policy.PolicyReasoner;
import com.ozu.ql.database.QueryTranslator;
import com.ozu.utils.StringConstants;


/**
 * Created by xx on 17/06/15.
 */
public class QLClassInference {
    public static boolean generateClassQueries() {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = null;
        HashMap<String, String> classQueryMap = new HashMap<String, String>();

        try {
            ontology = manager.loadOntologyFromOntologyDocument(new File(StringConstants.ONTOLOGY_PATH_RDF));

            QLQueryRewriter queryRewriter = QLQueryRewriter.getInstance();
            QueryTranslator translator = new QueryTranslator();

            Set<OWLClass> classes = ontology.getClassesInSignature();
            for (OWLClass owlClass : classes) {
                Query q = ConjunctiveQuery.mergeQueries(queryRewriter.rewriteQuery(queryRewriter.getMembershipQuery(owlClass.getIRI().getShortForm(), "x")));
                classQueryMap.put(owlClass.getIRI().getShortForm(), translator.convertSPARQL2SQL(q).toString());
            }

        } catch (OWLOntologyCreationException e) {
            System.err.println("Ontology Creation Exception: " + e.getMessage());
            return false;
        } catch (Exception ex) {
            System.err.println("Error occured while inferring classes: " + ex.getMessage());
            return false;
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(StringConstants.CLASS_QUERIES_TXT))) {
            bw.write(new GsonBuilder().create().toJson(classQueryMap));
        } catch (IOException e) {
            System.err.println("Error occured while writing class queries" + e.getMessage());
            return false;
        }

        return true;
    }

    public static HashMap<String, String> getClassQueries() {
        try {
            String content = new String(Files.readAllBytes(Paths.get(StringConstants.CLASS_QUERIES_TXT)));
            return new GsonBuilder().create().fromJson(content, new TypeToken<HashMap<String, String>>(){}.getType());
        } catch (IOException e) {
            System.err.println("Error occured while reading class queries" + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        generateClassQueries();
//    	
//    	try {
//			System.out.println(new ProblemGenerator().generateProblemFile());
//		} catch (OWLOntologyCreationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	PolicyManager.getInstance().readPolicies();
        QLReasoner reasoner = new ReasonerFactory().getReasoner();
        PolicyReasoner policyReasoner = new PolicyReasoner();
        policyReasoner.updateNormativeState();
        System.out.println(reasoner.isConsistent());

    }
}

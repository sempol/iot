package com.ozu.owlql;


import java.util.List;

import org.hibernate.Session;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.ibm.research.owlql.ConsistencyCheckResult;
import com.ibm.research.owlql.OWLQLCompiler;
import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.parser.LispExprList;
import com.ozu.ql.database.QueryTranslator;

/**
 * Created by xx on 02/04/15.
 */
public class QLReasoner {

	private OWLQLCompiler compiler = null;
	private boolean isSessionSet = false;
	private Session session;

	public QLReasoner() throws OWLOntologyCreationException {
		/* TODO Remove this part for SQL */
		this.session = null;
		this.isSessionSet = false;
		this.compiler = QLQueryRewriter.getInstance().getCompiler();
	}
	
	public void setSession(Session session) {
		this.session = session;
		this.isSessionSet = true;
	}

	public boolean isConsistent() {
		if (!isSessionSet)
			session = HibernateUtil.getSessionFactory().openSession();

		List result = null;
		try {
			/* TODO this can be stored somewhere */
			ConsistencyCheckResult computeConsistencyCheck = compiler.computeConsistencyCheck();
			String query = new QueryTranslator()
					.convertSPARQL2SQL(computeConsistencyCheck.getInconsistencyDetectionAskQuery()).toString();
			result = session.createSQLQuery(query).list();
		} catch (Exception e) {
			System.err.println("Exception occured during consistency check " + e);
			return false;
		} finally {
			if (!isSessionSet)
				session.close();
		}

		return (result == null || result.isEmpty());
	}

	/* TODO Returns policy costs, if inconsistent returns -1 */
	public boolean addPredicates(LispExprList statements) {
		// TODO Data Properties and Call function
		for (int i = 1; i < statements.size(); i++) {
			LispExprList property = (LispExprList) statements.get(i);

			if (property.size() == 2)
				addRemoveTypeProperty(property, false);
			else
				addRemoveObjectProperty(property, false);

			if (!this.isConsistent())
				return false;
		}
		return true;
	}

	public boolean deletePredicates(LispExprList statements) {
		for (int i = 1; i < statements.size(); i++) {
			LispExprList property = (LispExprList) statements.get(i);

			if (property.size() == 2)
				addRemoveTypeProperty(property, true);
			else
				addRemoveObjectProperty(property, true);

			if (!this.isConsistent())
				return false;
		}

		return true;
	}

	private void addRemoveTypeProperty(LispExprList p, boolean remove) {
		if (remove)
			deleteIndividual(p.get(1).toString());
		else
			addIndividual(p.get(1).toString(), p.get(0).toString());
	}

	private void addRemoveObjectProperty(LispExprList p, boolean remove) {
		if (remove)
			deleteObjectProperty(p.get(0).toString(), p.get(1).toString(), p.get(2).toString());
		else
			addObjectProperty(p.get(0).toString(), p.get(1).toString(), p.get(2).toString());
	}

	private void addIndividual(String name, String type) {
		System.out.println("QLReasoner - Adding individual: " + name + " type: " + type);
		if (Thing.addIndividualToSession(name, type, session) <= 0)
			System.err.println("Could not create new object " + name + " " + type);
	}

	private void deleteIndividual(String name) {
		System.out.println("QLReasoner - Deleting individual: " + name);
		Thing.deleteIndividualFromSession(name, session);
	}

	private void addObjectProperty(String predicate, String subjectName, String objectName) {
		System.out.println("QLReasoner - Adding object property: " + predicate + " subject: " + subjectName
				+ " object: " + objectName);
		if (ObjectProperty.addObjectPropertyToSession(predicate, subjectName, objectName, session) == 0) {
			System.err.println("Null Subject(" + subjectName + "): " + subjectName + " , Object(" + objectName + "): "
					+ objectName);
			return;
		}
	}

	private void deleteObjectProperty(String predicate, String subject, String object) {
		System.out.println(
				"QLReasoner - Deleting object property: " + predicate + " subject: " + subject + " object: " + object);
		int i = ObjectProperty.deleteObjectPropertyFromSession(predicate, subject, object, session);
		if (i <= 0)
			System.err.println("Could not create new property: " + subject + " " + predicate + " object");
	}

}

package com.ozu.owlql;

import org.hibernate.Session;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * Created by xx on 18/06/15.
 */
public class ReasonerFactory {

    public QLReasoner getReasoner() {
        try {
            return new QLReasoner();
        } catch (OWLOntologyCreationException e) {
            System.err.println("QLReasoner Constructor: " + e.getMessage());
            return null;
        }
    }

    public QLReasoner getReasonerWithSession(Session session) {
        QLReasoner qlReasoner = null;
        try {
            qlReasoner = new QLReasoner();
            qlReasoner.setSession(session);
            return qlReasoner;
        } catch (OWLOntologyCreationException e) {
            System.err.println("QLReasoner Constructor: " + e.getMessage());
            return null;
        }
    }

}

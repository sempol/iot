package com.ozu.planner;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import com.ozu.owlql.QL2JSHOP2;
import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;
import com.ozu.parser.LispParser;
import com.ozu.parser.LispTokenizer;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by xx on 31/03/15.
 */
public class DomainParser {
    private String domainPath;
    private LispExprList domain;
    private LispExprList actions;

    public void parseDomain(String domainPathIn) {
        domainPath = domainPathIn;

        try {
            LispTokenizer tzr = new LispTokenizer(readFile(domainPath, Charset.defaultCharset()));
            LispParser parser = new LispParser(tzr);
            LispParser.LispExpr lispExpr = parser.parseExpr();

            if (lispExpr instanceof LispExprList) {
                domain = (LispExprList) lispExpr;
                actions = getOperationsAndMethods();
            }
        } catch (IOException e) {
            System.err.println("Could not open the input file: " + e.toString());
        } catch (LispParser.ParseException e1) {
            System.err.println("Could not parse the domain file: " + e1.toString());
        }
    }

    public String rewritePreconditions() throws OWLOntologyCreationException {
        QL2JSHOP2 ql = new QL2JSHOP2();

        LispExprList newDomain = new LispExprList();

        newDomain.add(domain.get(0));
        newDomain.add(domain.get(1));

        LispExprList newActions = new LispExprList();

        for (int i = 0; i < actions.size(); i++) {
            if (isMethodOrOperator(actions.get(i))) {
                LispExprList action = (LispExprList) actions.get(i);
                LispExprList newAction = new LispExprList();

                newAction.add(action.get(0)); //op/method
                newAction.add(action.get(1)); //name

                if (isMethod(action)) {
                    boolean preconditionSeen = false;
                    for (int j = 2; j < action.size(); j++) {
                        LispParser.LispExpr lispExpr = action.get(j);

                        if (lispExpr instanceof Atom || preconditionSeen) {
                            preconditionSeen = false;
                            newAction.add(action.get(j));
                        } else if (!preconditionSeen) {
                            newAction.add(ql.rewritePredicate((LispExprList) action.get(j)));
                            preconditionSeen = true;
                        }
                    }
                } else {
                    //TODO Try statements like call.
                    newAction.add(ql.rewritePredicate((LispExprList) action.get(2))); //pred
                    newAction.add(action.get(3)); //add
                    newAction.add(action.get(4));

                    if (action.size() == 6) //if action has a cost
                        newAction.add(action.get(5));

                }

                newActions.add(newAction);
            } else {
                newActions.add(actions.get(i));
            }
        }

        newDomain.add(newActions);

        return newDomain.toString();
    }

    private boolean isMethod(LispParser.LispExpr e) {
        LispExprList isMethod = (LispExprList) e;

        if (isMethod.toString().contains("method"))
            return true;

        return false;
    }


    private boolean isMethodOrOperator(LispParser.LispExpr e) {
        if (!(e instanceof LispExprList))
            return false;

        LispExprList isAction = (LispExprList) e;

        if (!(isAction.get(0) instanceof Atom))
            return false;

        if (isAction.toString().contains("operator") || isAction.toString().contains("method"))
            return true;

        return false;
    }

    private LispExprList getOperationsAndMethods() {
        if (domain != null && domain.size() >= 3)
            if (domain.get(2) instanceof LispExprList)
                return ((LispExprList) domain.get(2));

        return null;
    }

    private String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}

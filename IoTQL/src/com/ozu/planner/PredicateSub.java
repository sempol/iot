package com.ozu.planner;

import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;

import java.util.ArrayList;
import java.util.List;

import static com.ozu.utils.StringConstants.RDF_SYNTAX_TYPE;

public class PredicateSub {
    private String object;
    private String subject;
    private String predicate;

    private List<String> resultVariables = new ArrayList<String>();

    public List<String> getResultVariables() {
        return resultVariables;
    }

    public void setResultVariables(List<String> resultVariables) {
        this.resultVariables = resultVariables;
    }

    private List<PredicateSub> alternatives = new ArrayList<PredicateSub>();

    private List<PredicateSub> adjacentStatements = new ArrayList<PredicateSub>();

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public void addAlternative(PredicateSub p) {
        this.alternatives.add(p);
    }

    public void addAdjacent(PredicateSub p) {
        this.adjacentStatements.add(p);
    }

    public LispExprList toSHOP2() {
        if (!alternatives.isEmpty()) {
            if (alternatives.size() == 1)
                return alternatives.get(0).toSHOP2();

            LispExprList exp = new LispExprList();
            exp.add(new Atom("or"));

            for (PredicateSub statement : alternatives)
                exp.add(statement.toSHOP2());

            return exp;
        } else if (!adjacentStatements.isEmpty()) {
            if (adjacentStatements.size() == 1)
                return adjacentStatements.get(0).toSHOP2();

            LispExprList exp = new LispExprList();

            for (PredicateSub statement : adjacentStatements)
                exp.add(statement.toSHOP2());

            return exp;
        } else {
            LispExprList exp = new LispExprList();
            boolean isType = false; // if it is a type e.g. door(?x) then it only has one parameter.

            if (this.predicate.contains(RDF_SYNTAX_TYPE)) {
                this.predicate = object.substring(object.lastIndexOf("#") + 1);
                isType = true;
            } else {
                this.predicate = predicate.substring(predicate.lastIndexOf("#") + 1);
            }

            if (!this.subject.contains("?"))
                subject = "?" + subject;

            exp.add(new Atom(predicate));
            exp.add(new Atom(subject));

            if (isType)
                return exp;

            if (!this.object.contains("?"))
                object = "?" + object;

            exp.add(new Atom(object));

            return exp;
        }
    }
}

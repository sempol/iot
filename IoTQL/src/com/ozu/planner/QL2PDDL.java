package com.ozu.planner;

import static com.ozu.utils.StringConstants.ONTOLOGY_PATH_RDF;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.hp.hpl.jena.graph.Triple;
import com.ibm.research.owlql.ConjunctiveQuery;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.owlql.QLQueryRewriter;
import com.ozu.parser.Atom;
import com.ozu.parser.LispExprList;
import com.ozu.parser.LispParser;

public class QL2PDDL {
	private QLQueryRewriter qr = QLQueryRewriter.getInstance();
	private OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	private OWLOntology ontology;
	private ArrayList<LispExprList> derivedAxioms = new ArrayList<LispExprList>();

	public QL2PDDL() throws OWLOntologyCreationException {
		ontology = manager.loadOntologyFromOntologyDocument(new File(ONTOLOGY_PATH_RDF));
	}

	public boolean generateDomainFile() throws OWLOntologyCreationException {
		LispExprList definition = new LispExprList();
		definition.add(new Atom("define"));

		LispExprList domain = new LispExprList();
		domain.add(new Atom("domain"));
		domain.add(new Atom("iot"));

		LispExprList requirements = new LispExprList();
		requirements.add(new Atom(":requirements"));
		requirements.add(new Atom(":adl"));
		requirements.add(new Atom(":derived-predicates"));

		/* TODO: Should we add Thing to the domain and action definitions? */
		/* TODO: Data properties */
		definition.add(domain);
		definition.add(requirements);
		definition.add(getPredicates());

		for (LispExprList derived : derivedAxioms)
			definition.add(derived);

		System.out.println(definition);

		return true;
	}

	public boolean generateProblemFile() throws OWLOntologyCreationException {
		LispExprList definition = new LispExprList();
		definition.add(new Atom("define"));

		LispExprList problem = new LispExprList();
		problem.add(new Atom("problem"));
		problem.add(new Atom("iot"));

		LispExprList domain = new LispExprList();
		domain.add(new Atom(":domain"));
		domain.add(new Atom("iot"));

		definition.add(problem);
		definition.add(domain);

		definition.add(getObjects());
		definition.add(getInitialState());
		definition.add(getGoalState());

		System.out.println(definition);

		return true;
	}

	private LispExprList getGoalState() {
		LispExprList goal = new LispExprList();
		goal.add(new Atom(":goal"));
		return goal;
	}

	private LispExprList getObjects() {
		LispExprList objects = new LispExprList();
		objects.add(new Atom(":objects"));
		List<String> individuals = Thing.getIndividualFromThing();
		for (String ind : individuals) {
			objects.add(new Atom(ind.replaceAll("[^A-Za-z0-9 ]", "")));
		}
		return objects;
	}

	private LispExprList getInitialState() {
		LispExprList initialState = new LispExprList();
		initialState.add(new Atom(":init"));

		LispExprList isConsistent = new LispExprList();
		isConsistent.add(new Atom("isConsistent"));
		
		Set<OWLClass> classes = ontology.getClassesInSignature();
		for (OWLClass owlClass : classes) {
			/* TODO do not insert all individuals into OWLThing */
			if (owlClass.isOWLThing())
				continue;

			List<String> individuals = Thing.getIndividualNames(owlClass.getIRI().getShortForm());
			for (String ind : individuals) {
				LispExprList individual = new LispExprList();
				individual.add(new Atom(owlClass.getIRI().getShortForm()));
				individual.add(new Atom(ind.replaceAll("[^A-Za-z0-9 ]", "")));
				initialState.add(individual);
			}
		}

		Set<OWLObjectProperty> properties = ontology.getObjectPropertiesInSignature();
		for (OWLObjectProperty property : properties) {
			List<Object[]> objectProperties = ObjectProperty.getObjectPropertiesFrom(property.getIRI().getShortForm());
			for (Object[] entry : objectProperties) {
				LispExprList objProperty = new LispExprList();
				objProperty.add(new Atom(property.getIRI().getShortForm()));
				objProperty.add(new Atom(Thing.getIndividualName((Integer) entry[0]).replaceAll("[^A-Za-z0-9 ]", "")));
				objProperty.add(new Atom(Thing.getIndividualName((Integer) entry[1]).replaceAll("[^A-Za-z0-9 ]", "")));
				initialState.add(objProperty);
			}
		}
		return initialState;
	}

	private LispExprList getPredicates() {
		LispExprList predicates = new LispExprList();
		predicates.add(new Atom(":predicates"));
		
		LispExprList isConsistent = new LispExprList();
		isConsistent.add(new Atom("isConsistent"));
		
		predicates.add(isConsistent);

		Set<OWLClass> classes = ontology.getClassesInSignature();
		for (OWLClass owlClass : classes) {
			LispExprList classPred = new LispExprList();

			String name = owlClass.getIRI().getShortForm();
			String var = ("" + owlClass.getIRI().getShortForm().charAt(0)).toLowerCase();

			classPred.add(new Atom(name));
			classPred.add(new Atom("?" + var));
			predicates.add(classPred);

			generateDomainAxioms(name, var, "", false);
		}

		Set<OWLObjectProperty> properties = ontology.getObjectPropertiesInSignature();
		for (OWLObjectProperty property : properties) {
			LispExprList propPred = new LispExprList();
			propPred.add(new Atom(property.getIRI().getShortForm()));
			propPred.add(new Atom("?s"));
			propPred.add(new Atom("?o"));
			predicates.add(propPred);

			generateDomainAxioms(property.getIRI().getShortForm(), "s", "o", true);
		}

		return predicates;
	}

	private void generateDomainAxioms(String pred, String subj, String obj, boolean isProperty) {
		Set<ConjunctiveQuery> nqs = null;

		if (isProperty)
			nqs = qr.rewriteQuery(qr.getPropertyQuery(pred, subj, obj));
		else
			nqs = qr.rewriteQuery(qr.getMembershipQuery(pred, subj));
		
		if (nqs.size() > 1) {
			LispExprList derived = new LispExprList();
			derived.add(new Atom(":derived"));

			LispExprList axiom = new LispExprList();
			axiom.add(new Atom(pred));
			axiom.add(new Atom("?" + subj));

			if (isProperty)
				axiom.add(new Atom("?" + obj));

			derived.add(axiom);

			LispExprList conditions = new LispExprList();
			if (nqs.size() > 2)
				conditions.add(new Atom("or"));

			for (ConjunctiveQuery qq : nqs) {

				List<Triple> triples = qq.getTriples();
				LispExprList rule = new LispExprList();
				if (triples.size() > 1)
					rule.add(new Atom("and"));

				boolean selfFlag = false;
				for (Triple triple : triples) {
					if (triple.getPredicate().getLocalName().equals("type")) {
						if (!triple.getObject().getLocalName().equals(pred)) {
							rule.add(new Atom(triple.getObject().getLocalName()));
							rule.add(new Atom(triple.getSubject().toString()));
						} else{
							selfFlag = true;
						}
					} else {
						rule.add(new Atom(triple.getPredicate().getLocalName()));
						rule.add(new Atom(triple.getSubject().toString()));
						rule.add(new Atom(triple.getObject().toString()));
					}
				}

				if (nqs.size() == 2 && !selfFlag)
					conditions = rule;
				else if (!selfFlag)
					conditions.add(rule);
			}

			derived.add(conditions);
			System.out.println(derived + "\n******************");
			derivedAxioms.add(derived);
		}
	}

}

package com.ozu.planner;

import java.util.ArrayList;
import java.util.HashMap;

public class ToyDevice {

	private String name;
	private ArrayList<String> capabilities;
	private HashMap<String, Integer> prohibitions;

	public ToyDevice(String name) {
		super();
		this.name = name;
		this.capabilities = new ArrayList<String>();
		this.prohibitions = new HashMap<String, Integer>();
	}

	public void addCapability(String cap) {
		capabilities.add(cap);
	}

	public void addProhibition(String cap, Integer cost) {
		prohibitions.put(cap, cost);
	}

	public String getName() {
		return name;
	}

	public ArrayList<String> getCapabilities() {
		return capabilities;
	}

	public HashMap<String, Integer> getProhibitions() {
		return prohibitions;
	}

	@Override
	public String toString() {
		String string = "Device: " + name + "\n";

		string += "Capabilities: ";
		for (int i = 0; i < capabilities.size(); i++) {
			string += capabilities.get(i);
			if (i != capabilities.size() - 1)
				string += ", ";
		}

		string += "\nProhibitions: ";
		for (String key : prohibitions.keySet()) {
			Integer cost = prohibitions.get(key);
			string += "(" + key + ", " + cost.toString() + ")";
		}

		return string;
	}

}

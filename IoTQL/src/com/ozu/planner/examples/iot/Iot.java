package com.ozu.planner.examples.iot;

import JSHOP2.*;

class Precondition0 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition0(Term[] unifier)
	{
		p = new Precondition[3];
		p[0] = new PreconditionAtomic(new Predicate(0, 10, new TermList(TermVariable.getVariable(1), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(1, 10, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[2] = new PreconditionAtomic(new Predicate(2, 10, new TermList(TermVariable.getVariable(4), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 3)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		whichClause = 0;
	}
}

class Precondition1 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition1(Term[] unifier)
	{
		p = new Precondition[8];
		p[0] = new PreconditionAtomic(new Predicate(4, 10, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(2), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(5, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		p[2] = new PreconditionAtomic(new Predicate(6, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(6), TermList.NIL))), unifier);

		p[3] = new PreconditionAtomic(new Predicate(7, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(7), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(8, 10, new TermList(TermVariable.getVariable(2), TermList.NIL)), unifier);

		p[5] = new PreconditionAtomic(new Predicate(9, 10, new TermList(TermVariable.getVariable(2), TermList.NIL)), unifier);

		p[6] = new PreconditionAtomic(new Predicate(2, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(8), TermList.NIL))), unifier);

		p[7] = new PreconditionAtomic(new Predicate(10, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(9), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
		p[5].bind(binding);
		p[6].bind(binding);
		p[7].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 8)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		whichClause = 0;
	}
}

class Precondition2 extends Precondition
{
	Precondition[] p;
	Term[][] b;

	public Precondition2(Term[] unifier)
	{
		p = new Precondition[5];
		p[1] = new Precondition0(unifier);
		p[2] = new PreconditionAtomic(new Predicate(3, 10, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);
		p[3] = new Precondition1(unifier);
		p[4] = new PreconditionAtomic(new Predicate(2, 10, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);
		b = new Term[5][];
		b[0] = unifier;
		b[0] = Term.merge( b, 1 );

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		b[0] = binding;
		b[0] = Term.merge( b, 1 );
		p[1].bind(binding);
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
	}

	protected Term[] nextBindingHelper()
	{
		while (b[4] == null)
		{
			boolean b3changed = false;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding();
						if (b[1] == null)
							return null;
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset();
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding();
					if (b[2] == null) b[1] = null;
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset();
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding();
				if (b[3] == null) b[2] = null;
				b3changed = true;
			}
			if ( b3changed ) {
				p[4].reset();
				p[4].bind(Term.merge(b, 4));
			}
			b[4] = p[4].nextBinding();
			if (b[4] == null) b[3] = null;
		}

		Term[] retVal = Term.merge(b, 5);
		b[4] = null;
		return retVal;
	}

	protected void resetHelper()
	{
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
	}
}

class Operator0 extends Operator
{
	public Operator0()
	{
		super(new Predicate(0, 10, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), new TermList(TermVariable.getVariable(2), TermList.NIL)))), -1, -1, new TermNumber(3.0));


		DelAddElement[] delIn = new DelAddElement[0];

		setDel(delIn);

		DelAddElement[] addIn = new DelAddElement[1];
		addIn[0] = new DelAddAtomic(new Predicate(11, 10, new TermList(TermVariable.getVariable(2), new TermList(new TermCall(new List(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), new TermList(TermVariable.getVariable(2), TermList.NIL))), Iot.calculateLocatePeopleInFlatImp, "Iot.calculateLocatePeopleInFlatImp"), TermList.NIL))));

		setAdd(addIn);
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		p = (new Precondition2(unifier)).setComparator(null);
		p.reset();

		return p;
	}
}

class Precondition3 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition3(Term[] unifier)
	{
		p = new Precondition[3];
		p[0] = new PreconditionAtomic(new Predicate(12, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(13, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(14, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 3)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		whichClause = 0;
	}
}

class Precondition4 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition4(Term[] unifier)
	{
		p = new Precondition[5];
		p[0] = new PreconditionAtomic(new Predicate(15, 6, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(16, 6, new TermList(TermVariable.getVariable(1), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(17, 6, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[3] = new PreconditionAtomic(new Predicate(7, 6, new TermList(TermVariable.getVariable(4), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(18, 6, new TermList(TermVariable.getVariable(1), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 5)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		whichClause = 0;
	}
}

class Precondition5 extends Precondition
{
	Precondition[] p;
	Term[][] b;

	public Precondition5(Term[] unifier)
	{
		p = new Precondition[3];
		p[1] = new Precondition3(unifier);
		p[2] = new Precondition4(unifier);
		b = new Term[3][];
		b[0] = unifier;
		b[0] = Term.merge( b, 1 );

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		b[0] = binding;
		b[0] = Term.merge( b, 1 );
		p[1].bind(binding);
		b[1] = null;
		b[2] = null;
	}

	protected Term[] nextBindingHelper()
	{
		while (b[2] == null)
		{
			boolean b1changed = false;
			while (b[1] == null)
			{
				b[1] = p[1].nextBinding();
				if (b[1] == null)
					return null;
				b1changed = true;
			}
			if ( b1changed ) {
				p[2].reset();
				p[2].bind(Term.merge(b, 2));
			}
			b[2] = p[2].nextBinding();
			if (b[2] == null) b[1] = null;
		}

		Term[] retVal = Term.merge(b, 3);
		b[2] = null;
		return retVal;
	}

	protected void resetHelper()
	{
		p[1].reset();
		p[2].reset();
		b[1] = null;
		b[2] = null;
	}
}

class Operator1 extends Operator
{
	public Operator1()
	{
		super(new Predicate(1, 6, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))), -1, -1, new TermNumber(3.0));


		DelAddElement[] delIn = new DelAddElement[0];

		setDel(delIn);

		DelAddElement[] addIn = new DelAddElement[0];

		setAdd(addIn);
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		p = (new Precondition5(unifier)).setComparator(null);
		p.reset();

		return p;
	}
}

class Precondition6 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition6(Term[] unifier)
	{
		p = new Precondition[4];
		p[0] = new PreconditionAtomic(new Predicate(19, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(20, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(21, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[3] = new PreconditionAtomic(new Predicate(13, 6, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 4)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		whichClause = 0;
	}
}

class Precondition7 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition7(Term[] unifier)
	{
		p = new Precondition[5];
		p[0] = new PreconditionAtomic(new Predicate(15, 6, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(16, 6, new TermList(TermVariable.getVariable(1), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(17, 6, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[3] = new PreconditionAtomic(new Predicate(7, 6, new TermList(TermVariable.getVariable(4), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(18, 6, new TermList(TermVariable.getVariable(1), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 5)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		whichClause = 0;
	}
}

class Precondition8 extends Precondition
{
	Precondition[] p;
	Term[][] b;

	public Precondition8(Term[] unifier)
	{
		p = new Precondition[3];
		p[1] = new Precondition6(unifier);
		p[2] = new Precondition7(unifier);
		b = new Term[3][];
		b[0] = unifier;
		b[0] = Term.merge( b, 1 );

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		b[0] = binding;
		b[0] = Term.merge( b, 1 );
		p[1].bind(binding);
		b[1] = null;
		b[2] = null;
	}

	protected Term[] nextBindingHelper()
	{
		while (b[2] == null)
		{
			boolean b1changed = false;
			while (b[1] == null)
			{
				b[1] = p[1].nextBinding();
				if (b[1] == null)
					return null;
				b1changed = true;
			}
			if ( b1changed ) {
				p[2].reset();
				p[2].bind(Term.merge(b, 2));
			}
			b[2] = p[2].nextBinding();
			if (b[2] == null) b[1] = null;
		}

		Term[] retVal = Term.merge(b, 3);
		b[2] = null;
		return retVal;
	}

	protected void resetHelper()
	{
		p[1].reset();
		p[2].reset();
		b[1] = null;
		b[2] = null;
	}
}

class Operator2 extends Operator
{
	public Operator2()
	{
		super(new Predicate(2, 6, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))), -1, -1, new TermNumber(4.0));


		DelAddElement[] delIn = new DelAddElement[0];

		setDel(delIn);

		DelAddElement[] addIn = new DelAddElement[0];

		setAdd(addIn);
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		p = (new Precondition8(unifier)).setComparator(null);
		p.reset();

		return p;
	}
}

class Precondition9 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition9(Term[] unifier)
	{
		p = new Precondition[3];
		p[0] = new PreconditionAtomic(new Predicate(12, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(13, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(14, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 3)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		whichClause = 0;
	}
}

class Precondition10 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition10(Term[] unifier)
	{
		p = new Precondition[8];
		p[0] = new PreconditionAtomic(new Predicate(4, 18, new TermList(TermVariable.getVariable(4), new TermList(TermVariable.getVariable(0), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(5, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		p[2] = new PreconditionAtomic(new Predicate(6, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(6), TermList.NIL))), unifier);

		p[3] = new PreconditionAtomic(new Predicate(7, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(7), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(8, 18, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[5] = new PreconditionAtomic(new Predicate(9, 18, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[6] = new PreconditionAtomic(new Predicate(2, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(8), TermList.NIL))), unifier);

		p[7] = new PreconditionAtomic(new Predicate(10, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(9), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
		p[5].bind(binding);
		p[6].bind(binding);
		p[7].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 8)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		whichClause = 0;
	}
}

class Precondition11 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition11(Term[] unifier)
	{
		p = new Precondition[16];
		p[0] = new PreconditionAtomic(new Predicate(23, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(11), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(24, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(25, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[3] = new PreconditionAtomic(new Predicate(26, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(27, 18, new TermList(TermVariable.getVariable(12), new TermList(TermVariable.getVariable(10), TermList.NIL))), unifier);

		p[5] = new PreconditionAtomic(new Predicate(28, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(4), TermList.NIL))), unifier);

		p[6] = new PreconditionAtomic(new Predicate(29, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[7] = new PreconditionAtomic(new Predicate(30, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[8] = new PreconditionAtomic(new Predicate(31, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[9] = new PreconditionAtomic(new Predicate(32, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(13), TermList.NIL))), unifier);

		p[10] = new PreconditionAtomic(new Predicate(33, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[11] = new PreconditionAtomic(new Predicate(34, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(9), TermList.NIL))), unifier);

		p[12] = new PreconditionAtomic(new Predicate(35, 18, new TermList(TermVariable.getVariable(14), new TermList(TermVariable.getVariable(10), TermList.NIL))), unifier);

		p[13] = new PreconditionAtomic(new Predicate(36, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[14] = new PreconditionAtomic(new Predicate(37, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(7), TermList.NIL))), unifier);

		p[15] = new PreconditionAtomic(new Predicate(27, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(15), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
		p[5].bind(binding);
		p[6].bind(binding);
		p[7].bind(binding);
		p[8].bind(binding);
		p[9].bind(binding);
		p[10].bind(binding);
		p[11].bind(binding);
		p[12].bind(binding);
		p[13].bind(binding);
		p[14].bind(binding);
		p[15].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 16)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		p[8].reset();
		p[9].reset();
		p[10].reset();
		p[11].reset();
		p[12].reset();
		p[13].reset();
		p[14].reset();
		p[15].reset();
		whichClause = 0;
	}
}

class Precondition12 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition12(Term[] unifier)
	{
		p = new Precondition[2];
		p[0] = new PreconditionAtomic(new Predicate(38, 18, new TermList(TermVariable.getVariable(16), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(14), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 2)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		whichClause = 0;
	}
}

class Precondition13 extends Precondition
{
	Precondition[] p;
	Term[][] b;

	public Precondition13(Term[] unifier)
	{
		p = new Precondition[12];
		p[1] = new PreconditionAtomic(new Predicate(22, 18, new TermList(TermVariable.getVariable(2), TermList.NIL)), unifier);
		p[2] = new Precondition9(unifier);
		p[3] = new Precondition10(unifier);
		p[4] = new Precondition11(unifier);
		p[5] = new Precondition12(unifier);
		p[6] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier), 18);
		p[7] = new PreconditionAtomic(new Predicate(34, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(3), TermList.NIL))), unifier);
		p[8] = new PreconditionAtomic(new Predicate(39, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(2), TermList.NIL))), unifier);
		p[9] = new PreconditionAtomic(new Predicate(1, 18, new TermList(TermVariable.getVariable(16), new TermList(TermVariable.getVariable(17), TermList.NIL))), unifier);
		p[10] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);
		p[11] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);
		b = new Term[12][];
		b[0] = unifier;
		b[0] = Term.merge( b, 1 );

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		b[0] = binding;
		b[0] = Term.merge( b, 1 );
		p[1].bind(binding);
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
		b[5] = null;
		b[6] = null;
		b[7] = null;
		b[8] = null;
		b[9] = null;
		b[10] = null;
		b[11] = null;
	}

	protected Term[] nextBindingHelper()
	{
		while (b[11] == null)
		{
			boolean b10changed = false;
			while (b[10] == null)
			{
				boolean b9changed = false;
				while (b[9] == null)
				{
					boolean b8changed = false;
					while (b[8] == null)
					{
						boolean b7changed = false;
						while (b[7] == null)
						{
							boolean b6changed = false;
							while (b[6] == null)
							{
								boolean b5changed = false;
								while (b[5] == null)
								{
									boolean b4changed = false;
									while (b[4] == null)
									{
										boolean b3changed = false;
										while (b[3] == null)
										{
											boolean b2changed = false;
											while (b[2] == null)
											{
												boolean b1changed = false;
												while (b[1] == null)
												{
													b[1] = p[1].nextBinding();
													if (b[1] == null)
														return null;
													b1changed = true;
												}
												if ( b1changed ) {
													p[2].reset();
													p[2].bind(Term.merge(b, 2));
												}
												b[2] = p[2].nextBinding();
												if (b[2] == null) b[1] = null;
												b2changed = true;
											}
											if ( b2changed ) {
												p[3].reset();
												p[3].bind(Term.merge(b, 3));
											}
											b[3] = p[3].nextBinding();
											if (b[3] == null) b[2] = null;
											b3changed = true;
										}
										if ( b3changed ) {
											p[4].reset();
											p[4].bind(Term.merge(b, 4));
										}
										b[4] = p[4].nextBinding();
										if (b[4] == null) b[3] = null;
										b4changed = true;
									}
									if ( b4changed ) {
										p[5].reset();
										p[5].bind(Term.merge(b, 5));
									}
									b[5] = p[5].nextBinding();
									if (b[5] == null) b[4] = null;
									b5changed = true;
								}
								if ( b5changed ) {
									p[6].reset();
									p[6].bind(Term.merge(b, 6));
								}
								b[6] = p[6].nextBinding();
								if (b[6] == null) b[5] = null;
								b6changed = true;
							}
							if ( b6changed ) {
								p[7].reset();
								p[7].bind(Term.merge(b, 7));
							}
							b[7] = p[7].nextBinding();
							if (b[7] == null) b[6] = null;
							b7changed = true;
						}
						if ( b7changed ) {
							p[8].reset();
							p[8].bind(Term.merge(b, 8));
						}
						b[8] = p[8].nextBinding();
						if (b[8] == null) b[7] = null;
						b8changed = true;
					}
					if ( b8changed ) {
						p[9].reset();
						p[9].bind(Term.merge(b, 9));
					}
					b[9] = p[9].nextBinding();
					if (b[9] == null) b[8] = null;
					b9changed = true;
				}
				if ( b9changed ) {
					p[10].reset();
					p[10].bind(Term.merge(b, 10));
				}
				b[10] = p[10].nextBinding();
				if (b[10] == null) b[9] = null;
				b10changed = true;
			}
			if ( b10changed ) {
				p[11].reset();
				p[11].bind(Term.merge(b, 11));
			}
			b[11] = p[11].nextBinding();
			if (b[11] == null) b[10] = null;
		}

		Term[] retVal = Term.merge(b, 12);
		b[11] = null;
		return retVal;
	}

	protected void resetHelper()
	{
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		p[8].reset();
		p[9].reset();
		p[10].reset();
		p[11].reset();
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
		b[5] = null;
		b[6] = null;
		b[7] = null;
		b[8] = null;
		b[9] = null;
		b[10] = null;
		b[11] = null;
	}
}

class Method0 extends Method
{
	public Method0()
	{
		super(new Predicate(0, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))));
		TaskList[] subsIn = new TaskList[1];

		subsIn[0] = createTaskList0();

		setSubs(subsIn);
	}

	TaskList createTaskList0()
	{
		TaskList retVal;

		retVal = new TaskList(1, true);
		retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 18, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(1), TermList.NIL))), false, true));

		return retVal;
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		switch (which)
		{
			case 0:
				p = (new Precondition13(unifier)).setComparator(null);
			break;
			default:
				return null;
		}

		p.reset();

		return p;
	}

	public String getLabel(int which)
	{
		switch (which)
		{
			case 0: return "Method0Branch0";
			default: return null;
		}
	}
}

class Precondition14 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition14(Term[] unifier)
	{
		p = new Precondition[4];
		p[0] = new PreconditionAtomic(new Predicate(19, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(20, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(21, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		p[3] = new PreconditionAtomic(new Predicate(13, 18, new TermList(TermVariable.getVariable(3), TermList.NIL)), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 4)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		whichClause = 0;
	}
}

class Precondition15 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition15(Term[] unifier)
	{
		p = new Precondition[8];
		p[0] = new PreconditionAtomic(new Predicate(4, 18, new TermList(TermVariable.getVariable(4), new TermList(TermVariable.getVariable(0), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(5, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		p[2] = new PreconditionAtomic(new Predicate(6, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(6), TermList.NIL))), unifier);

		p[3] = new PreconditionAtomic(new Predicate(7, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(7), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(8, 18, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[5] = new PreconditionAtomic(new Predicate(9, 18, new TermList(TermVariable.getVariable(0), TermList.NIL)), unifier);

		p[6] = new PreconditionAtomic(new Predicate(2, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(8), TermList.NIL))), unifier);

		p[7] = new PreconditionAtomic(new Predicate(10, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(9), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
		p[5].bind(binding);
		p[6].bind(binding);
		p[7].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 8)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		whichClause = 0;
	}
}

class Precondition16 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition16(Term[] unifier)
	{
		p = new Precondition[16];
		p[0] = new PreconditionAtomic(new Predicate(23, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(11), TermList.NIL))), unifier);

		p[1] = new PreconditionAtomic(new Predicate(24, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[2] = new PreconditionAtomic(new Predicate(25, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[3] = new PreconditionAtomic(new Predicate(26, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(5), TermList.NIL))), unifier);

		p[4] = new PreconditionAtomic(new Predicate(27, 18, new TermList(TermVariable.getVariable(12), new TermList(TermVariable.getVariable(10), TermList.NIL))), unifier);

		p[5] = new PreconditionAtomic(new Predicate(28, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(4), TermList.NIL))), unifier);

		p[6] = new PreconditionAtomic(new Predicate(29, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[7] = new PreconditionAtomic(new Predicate(30, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[8] = new PreconditionAtomic(new Predicate(31, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[9] = new PreconditionAtomic(new Predicate(32, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(13), TermList.NIL))), unifier);

		p[10] = new PreconditionAtomic(new Predicate(33, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[11] = new PreconditionAtomic(new Predicate(34, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(9), TermList.NIL))), unifier);

		p[12] = new PreconditionAtomic(new Predicate(35, 18, new TermList(TermVariable.getVariable(14), new TermList(TermVariable.getVariable(10), TermList.NIL))), unifier);

		p[13] = new PreconditionAtomic(new Predicate(36, 18, new TermList(TermVariable.getVariable(10), TermList.NIL)), unifier);

		p[14] = new PreconditionAtomic(new Predicate(37, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(7), TermList.NIL))), unifier);

		p[15] = new PreconditionAtomic(new Predicate(27, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(15), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
		p[2].bind(binding);
		p[3].bind(binding);
		p[4].bind(binding);
		p[5].bind(binding);
		p[6].bind(binding);
		p[7].bind(binding);
		p[8].bind(binding);
		p[9].bind(binding);
		p[10].bind(binding);
		p[11].bind(binding);
		p[12].bind(binding);
		p[13].bind(binding);
		p[14].bind(binding);
		p[15].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 16)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		p[8].reset();
		p[9].reset();
		p[10].reset();
		p[11].reset();
		p[12].reset();
		p[13].reset();
		p[14].reset();
		p[15].reset();
		whichClause = 0;
	}
}

class Precondition17 extends Precondition
{
	Precondition[] p;
	Term[] b;
	int whichClause;

	public Precondition17(Term[] unifier)
	{
		p = new Precondition[2];
		p[0] = new PreconditionAtomic(new Predicate(38, 18, new TermList(TermVariable.getVariable(16), TermList.NIL)), unifier);

		p[1] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(14), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		p[0].bind(binding);
		p[1].bind(binding);
	}

	protected Term[] nextBindingHelper()
	{
		while (whichClause < 2)
		{
			b = p[whichClause].nextBinding();
			if (b != null)
				 return b;
			whichClause++;
		}

		return null;
	}

	protected void resetHelper()
	{
		p[0].reset();
		p[1].reset();
		whichClause = 0;
	}
}

class Precondition18 extends Precondition
{
	Precondition[] p;
	Term[][] b;

	public Precondition18(Term[] unifier)
	{
		p = new Precondition[12];
		p[1] = new PreconditionAtomic(new Predicate(40, 18, new TermList(TermVariable.getVariable(2), TermList.NIL)), unifier);
		p[2] = new Precondition14(unifier);
		p[3] = new Precondition15(unifier);
		p[4] = new Precondition16(unifier);
		p[5] = new Precondition17(unifier);
		p[6] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))), unifier), 18);
		p[7] = new PreconditionAtomic(new Predicate(34, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(3), TermList.NIL))), unifier);
		p[8] = new PreconditionAtomic(new Predicate(39, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(2), TermList.NIL))), unifier);
		p[9] = new PreconditionAtomic(new Predicate(1, 18, new TermList(TermVariable.getVariable(16), new TermList(TermVariable.getVariable(17), TermList.NIL))), unifier);
		p[10] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(10), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);
		p[11] = new PreconditionAtomic(new Predicate(11, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(16), TermList.NIL))), unifier);
		b = new Term[12][];
		b[0] = unifier;
		b[0] = Term.merge( b, 1 );

		setFirst(false);
	}

	public void bind(Term[] binding)
	{
		b[0] = binding;
		b[0] = Term.merge( b, 1 );
		p[1].bind(binding);
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
		b[5] = null;
		b[6] = null;
		b[7] = null;
		b[8] = null;
		b[9] = null;
		b[10] = null;
		b[11] = null;
	}

	protected Term[] nextBindingHelper()
	{
		while (b[11] == null)
		{
			boolean b10changed = false;
			while (b[10] == null)
			{
				boolean b9changed = false;
				while (b[9] == null)
				{
					boolean b8changed = false;
					while (b[8] == null)
					{
						boolean b7changed = false;
						while (b[7] == null)
						{
							boolean b6changed = false;
							while (b[6] == null)
							{
								boolean b5changed = false;
								while (b[5] == null)
								{
									boolean b4changed = false;
									while (b[4] == null)
									{
										boolean b3changed = false;
										while (b[3] == null)
										{
											boolean b2changed = false;
											while (b[2] == null)
											{
												boolean b1changed = false;
												while (b[1] == null)
												{
													b[1] = p[1].nextBinding();
													if (b[1] == null)
														return null;
													b1changed = true;
												}
												if ( b1changed ) {
													p[2].reset();
													p[2].bind(Term.merge(b, 2));
												}
												b[2] = p[2].nextBinding();
												if (b[2] == null) b[1] = null;
												b2changed = true;
											}
											if ( b2changed ) {
												p[3].reset();
												p[3].bind(Term.merge(b, 3));
											}
											b[3] = p[3].nextBinding();
											if (b[3] == null) b[2] = null;
											b3changed = true;
										}
										if ( b3changed ) {
											p[4].reset();
											p[4].bind(Term.merge(b, 4));
										}
										b[4] = p[4].nextBinding();
										if (b[4] == null) b[3] = null;
										b4changed = true;
									}
									if ( b4changed ) {
										p[5].reset();
										p[5].bind(Term.merge(b, 5));
									}
									b[5] = p[5].nextBinding();
									if (b[5] == null) b[4] = null;
									b5changed = true;
								}
								if ( b5changed ) {
									p[6].reset();
									p[6].bind(Term.merge(b, 6));
								}
								b[6] = p[6].nextBinding();
								if (b[6] == null) b[5] = null;
								b6changed = true;
							}
							if ( b6changed ) {
								p[7].reset();
								p[7].bind(Term.merge(b, 7));
							}
							b[7] = p[7].nextBinding();
							if (b[7] == null) b[6] = null;
							b7changed = true;
						}
						if ( b7changed ) {
							p[8].reset();
							p[8].bind(Term.merge(b, 8));
						}
						b[8] = p[8].nextBinding();
						if (b[8] == null) b[7] = null;
						b8changed = true;
					}
					if ( b8changed ) {
						p[9].reset();
						p[9].bind(Term.merge(b, 9));
					}
					b[9] = p[9].nextBinding();
					if (b[9] == null) b[8] = null;
					b9changed = true;
				}
				if ( b9changed ) {
					p[10].reset();
					p[10].bind(Term.merge(b, 10));
				}
				b[10] = p[10].nextBinding();
				if (b[10] == null) b[9] = null;
				b10changed = true;
			}
			if ( b10changed ) {
				p[11].reset();
				p[11].bind(Term.merge(b, 11));
			}
			b[11] = p[11].nextBinding();
			if (b[11] == null) b[10] = null;
		}

		Term[] retVal = Term.merge(b, 12);
		b[11] = null;
		return retVal;
	}

	protected void resetHelper()
	{
		p[1].reset();
		p[2].reset();
		p[3].reset();
		p[4].reset();
		p[5].reset();
		p[6].reset();
		p[7].reset();
		p[8].reset();
		p[9].reset();
		p[10].reset();
		p[11].reset();
		b[1] = null;
		b[2] = null;
		b[3] = null;
		b[4] = null;
		b[5] = null;
		b[6] = null;
		b[7] = null;
		b[8] = null;
		b[9] = null;
		b[10] = null;
		b[11] = null;
	}
}

class Method1 extends Method
{
	public Method1()
	{
		super(new Predicate(1, 18, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))));
		TaskList[] subsIn = new TaskList[1];

		subsIn[0] = createTaskList0();

		setSubs(subsIn);
	}

	TaskList createTaskList0()
	{
		TaskList retVal;

		retVal = new TaskList(1, true);
		retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 18, new TermList(TermVariable.getVariable(3), new TermList(TermVariable.getVariable(1), TermList.NIL))), false, true));

		return retVal;
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		switch (which)
		{
			case 0:
				p = (new Precondition18(unifier)).setComparator(null);
			break;
			default:
				return null;
		}

		p.reset();

		return p;
	}

	public String getLabel(int which)
	{
		switch (which)
		{
			case 0: return "Method1Branch0";
			default: return null;
		}
	}
}

class Method2 extends Method
{
	public Method2()
	{
		super(new Predicate(3, 3, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))));
		TaskList[] subsIn = new TaskList[1];

		subsIn[0] = createTaskList0();

		setSubs(subsIn);
	}

	TaskList createTaskList0()
	{
		TaskList retVal;

		retVal = new TaskList(2, true);
		retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(TermVariable.getVariable(0), TermList.NIL)), false, false));
		retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(1), TermList.NIL))), false, false));

		return retVal;
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		switch (which)
		{
			case 0:
				p = (new PreconditionNil(3)).setComparator(null);
			break;
			default:
				return null;
		}

		p.reset();

		return p;
	}

	public String getLabel(int which)
	{
		switch (which)
		{
			case 0: return "Method2Branch0";
			default: return null;
		}
	}
}

class Method3 extends Method
{
	public Method3()
	{
		super(new Predicate(3, 3, new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(1), TermList.NIL))));
		TaskList[] subsIn = new TaskList[1];

		subsIn[0] = createTaskList0();

		setSubs(subsIn);
	}

	TaskList createTaskList0()
	{
		TaskList retVal;

		retVal = new TaskList(2, true);
		retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(TermVariable.getVariable(0), TermList.NIL)), false, false));
		retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(TermVariable.getVariable(2), new TermList(TermVariable.getVariable(1), TermList.NIL))), false, false));

		return retVal;
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		switch (which)
		{
			case 0:
				p = (new PreconditionNil(3)).setComparator(null);
			break;
			default:
				return null;
		}

		p.reset();

		return p;
	}

	public String getLabel(int which)
	{
		switch (which)
		{
			case 0: return "Method3Branch0";
			default: return null;
		}
	}
}

class Method4 extends Method
{
	public Method4()
	{
		super(new Predicate(2, 3, new TermList(TermVariable.getVariable(0), TermList.NIL)));
		TaskList[] subsIn = new TaskList[1];

		subsIn[0] = createTaskList0();

		setSubs(subsIn);
	}

	TaskList createTaskList0()
	{
		TaskList retVal;

		retVal = new TaskList(1, true);
		retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(TermVariable.getVariable(1), new TermList(TermVariable.getVariable(0), new TermList(TermVariable.getVariable(2), TermList.NIL)))), false, true));

		return retVal;
	}

	public Precondition getIterator(Term[] unifier, int which)
	{
		Precondition p;

		switch (which)
		{
			case 0:
				p = (new PreconditionNil(3)).setComparator(null);
			break;
			default:
				return null;
		}

		p.reset();

		return p;
	}

	public String getLabel(int which)
	{
		switch (which)
		{
			case 0: return "Method4Branch0";
			default: return null;
		}
	}
}

public class Iot extends Domain
{
	public static LocatePeopleInFlatImp calculateLocatePeopleInFlatImp = new LocatePeopleInFlatImp();

	public Iot()
	{
		TermVariable.initialize(18);

		constants = new String[41];
		constants[0] = "Flat";
		constants[1] = "belongsTo";
		constants[2] = "residesIn";
		constants[3] = "LocatePeopleInFlat";
		constants[4] = "hasReceiver";
		constants[5] = "getNotifiedWith";
		constants[6] = "disabledNotificationType";
		constants[7] = "gotNotifiedFor";
		constants[8] = "Person";
		constants[9] = "Mother";
		constants[10] = "enabledNotificationType";
		constants[11] = "inRoom";
		constants[12] = "SoundAction";
		constants[13] = "NotifyAction";
		constants[14] = "PlaySound";
		constants[15] = "hasEvent";
		constants[16] = "Event";
		constants[17] = "produces";
		constants[18] = "producedBy";
		constants[19] = "DisplayVisualMessage";
		constants[20] = "TurnOnLights";
		constants[21] = "VisualAction";
		constants[22] = "SoundNotification";
		constants[23] = "hasNetworkInterface";
		constants[24] = "Screen";
		constants[25] = "Doorbell";
		constants[26] = "isOnPlatform";
		constants[27] = "hasSubsystem";
		constants[28] = "hasPowerLevel";
		constants[29] = "ElectronicAgent";
		constants[30] = "Device";
		constants[31] = "Speaker";
		constants[32] = "canNotifyWith";
		constants[33] = "Sensor";
		constants[34] = "canPerform";
		constants[35] = "hasSender";
		constants[36] = "TemperatureSensor";
		constants[37] = "performedAction";
		constants[38] = "Room";
		constants[39] = "canBeNotifiedWith";
		constants[40] = "VisualNotification";

		compoundTasks = new String[4];
		compoundTasks[0] = "notify-person-with-sound";
		compoundTasks[1] = "notify-person-with-visual";
		compoundTasks[2] = "locate-people-in-flat";
		compoundTasks[3] = "notify-people-in-flat";

		primitiveTasks = new String[3];
		primitiveTasks[0] = "!locate-people-with-beacons";
		primitiveTasks[1] = "!play-sound";
		primitiveTasks[2] = "!display-visual-message";

		methods = new Method[4][];

		methods[0] = new Method[1];
		methods[0][0] = new Method0();

		methods[1] = new Method[1];
		methods[1][0] = new Method1();

		methods[2] = new Method[1];
		methods[2][0] = new Method4();

		methods[3] = new Method[2];
		methods[3][0] = new Method2();
		methods[3][1] = new Method3();


		ops = new Operator[3][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0();

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1();

		ops[2] = new Operator[1];
		ops[2][0] = new Operator2();

		axioms = new Axiom[41][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[0];

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[0];

		axioms[11] = new Axiom[0];

		axioms[12] = new Axiom[0];

		axioms[13] = new Axiom[0];

		axioms[14] = new Axiom[0];

		axioms[15] = new Axiom[0];

		axioms[16] = new Axiom[0];

		axioms[17] = new Axiom[0];

		axioms[18] = new Axiom[0];

		axioms[19] = new Axiom[0];

		axioms[20] = new Axiom[0];

		axioms[21] = new Axiom[0];

		axioms[22] = new Axiom[0];

		axioms[23] = new Axiom[0];

		axioms[24] = new Axiom[0];

		axioms[25] = new Axiom[0];

		axioms[26] = new Axiom[0];

		axioms[27] = new Axiom[0];

		axioms[28] = new Axiom[0];

		axioms[29] = new Axiom[0];

		axioms[30] = new Axiom[0];

		axioms[31] = new Axiom[0];

		axioms[32] = new Axiom[0];

		axioms[33] = new Axiom[0];

		axioms[34] = new Axiom[0];

		axioms[35] = new Axiom[0];

		axioms[36] = new Axiom[0];

		axioms[37] = new Axiom[0];

		axioms[38] = new Axiom[0];

		axioms[39] = new Axiom[0];

		axioms[40] = new Axiom[0];

	}
}

package com.ozu.planner.examples.iot;

import java.util.HashMap;

import JSHOP2.Calculate;
import JSHOP2.List;
import JSHOP2.Term;
import JSHOP2.TermConstant;

public class LocatePeopleInFlatImp implements Calculate {
	/* also can use hashes of term ids */
	private static HashMap<String, Term> cache = new HashMap<String, Term>();
	
	@Override
	public Term call(List l) {
		
		if (cache.containsKey(l.toString()))
			return cache.get(l.toString());
		
		String room = "room1";
		Term ret = TermConstant.getConstantWithName(room);
		// TODO run the information gathering action and return the result
		/* TODO If actual call returns a Term that does not exist, what to do? */
		
		cache.put(l.toString(), ret);
		return TermConstant.getConstantWithName(room);
	}

}

package com.ozu.planner.examples.iot;

import JSHOP2.*;
import antlr.RecognitionException;
import antlr.TokenStreamException;
import com.ozu.planner.DomainParser;
import com.ozu.planner.ProblemGenerator;
import com.ozu.planner.QL2PDDL;
import com.ozu.policy.PolicyGoal;

import JSHOP2.InternalDomain;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static com.ozu.utils.StringConstants.PLANNER_INPUT_FOLDER;

public class PlannerJSHOP2 {
	/* TODO remove static from function definitions after testing */
	/* TODO re-write causes run time error and duplicate plans with JSHOP2 */
	private void reWriteDomainFile(String path) {
		File myFoo = new File(path + "_new");
		FileOutputStream fooStream = null; // true to append
		try {
			DomainParser parser = new DomainParser();
			parser.parseDomain(path);
			byte[] myBytes = parser.rewritePreconditions().getBytes();
			fooStream = new FileOutputStream(myFoo, false);
			fooStream.write(myBytes);
			fooStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
	}

	private void generateDomainJava()
			throws FileNotFoundException, IOException, RecognitionException, TokenStreamException {
		String domainPath = "/Users/xx/Documents/workspace/IoT/IoTPlanner/examples/iot/iot";
		String newDomainPath = "/Users/xx/Documents/workspace/IoT/IoTPlanner/examples/iot/iot_new";
		reWriteDomainFile(domainPath);
		InternalDomain d = new InternalDomain(new FileInputStream(new File(newDomainPath)), -1);
		d.getParser().domain();
	}

	private void generateProblemJava(PolicyGoal goal) throws FileNotFoundException, IOException, RecognitionException,
			TokenStreamException, OWLOntologyCreationException, ClassNotFoundException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		String problemPath = "/Users/xx/Documents/workspace/IoT/IoTPlanner/examples/iot/problem";
		Files.write(Paths.get(problemPath), (new ProblemGenerator().generateProblemFile(goal)).getBytes());
		InternalDomain p = new InternalDomain(new FileInputStream(new File(problemPath)), Integer.MAX_VALUE);
		p.getParser().command();
	}

	public void reloadClasses() throws IOException {
		JSHOP2InputRecompiler.compile();
	}

	public void invokeGetPlans()
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, MalformedURLException, IOException, InvocationTargetException {
		File source = new File(PLANNER_INPUT_FOLDER);
		try (URLClassLoader cl = new URLClassLoader(new URL[] { source.toURI().toURL() })) {
			Class<?> cls = (Class<?>) cl.loadClass("com.ozu.planner.examples.iot.Problem");
			Method method = cls.getMethod("getPlans");
			Object[] args = {};
			method.invoke(null, args);
		}
	}

	public void startPlanner(PolicyGoal goal) throws FileNotFoundException, RecognitionException, TokenStreamException,
			IOException, OWLOntologyCreationException, ClassNotFoundException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

		generateDomainJava();
		generateProblemJava(goal);
		reloadClasses();
		invokeGetPlans();
		/*
		 * TODO problem produces duplicate plans. JSHOP2QL runallsteps is
		 * redundant. We should be able to get all the steps during planning
		 */
		new JSHOP2QLInterface().runAllSteps();
	}

	public static void main(String[] args) throws Exception {
		try {
			String action = "notify-people-in-flat";
			ArrayList<String> list = new ArrayList<String>();
			list.add("flat1");
			list.add("someoneAtFrontDoor");
			
			new QL2PDDL().generateDomainFile();
			new QL2PDDL().generateProblemFile();
			/* switch back to datalogcompiler when this is done */
//			new PlannerJSHOP2().startPlanner(new PolicyGoal(action, list));
		} catch (Exception e) {
			System.err.println("Planner error: " + e.getMessage());
			throw e;
		}

	}
}

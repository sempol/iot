package com.ozu.policy;

import org.hibernate.Session;
import org.hibernate.engine.jdbc.internal.BasicFormatterImpl;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ql.database.SQLQuery;

import java.util.*;

/**
 * Created by xx on 14/04/15.
 */
public class ActivePolicy {
	private String addressee;
	private String modality;
	private String name;

	private SQLQuery activation;
	private SQLQuery expiration;
	private SQLQuery deadline;

	private HashMap<String, String> bindings;

	public String getName() {
		return name;
	}

	private double cost;

	private List<PolicyGoal> goal;

	public ActivePolicy(String addressee, String modality, String name, double cost, SQLQuery activation,
			SQLQuery expiration, SQLQuery deadline, List<PolicyGoal> goal, HashMap<String, String> bindings) {
		this.addressee = addressee;
		this.modality = modality;
		this.name = name;
		this.activation = activation;
		this.expiration = expiration;
		this.deadline = deadline;
		this.cost = cost;
		this.goal = goal;
		this.bindings = bindings;
	}

	public ActivePolicy cloneActivePolicy() {
		List<PolicyGoal> _goal = new ArrayList<PolicyGoal>();
		for (PolicyGoal g : goal)
			_goal.add(g.clonePolicyGoal());

		return new ActivePolicy(addressee, modality, name, cost, activation.cloneQuery(), expiration.cloneQuery(),
				deadline.cloneQuery(), _goal, new HashMap<String, String>(bindings));
	}

	public boolean checkViolation(PolicyGoal target, Session session) {
		boolean result = false;
		for (PolicyGoal g : goal)
			if (g.equals(target))
				result = true;

		if (result && modality.contains("Obligation"))
			return checkDeadline(session);

		return result;
	}

	public double getCost() {
		return cost;
	}

	public boolean isExpired() {
		return queryHasResults(expiration, null);
	}

	public boolean isExpired(Session session) {
		return queryHasResults(expiration, session);
	}

	public boolean checkDeadline() {
		if (!modality.contains("owlpolar:'Obligation'"))
			return false;
		return queryHasResults(deadline, null);
	}

	public boolean checkDeadline(Session session) {
		if (!modality.contains("owlpolar:'Obligation'"))
			return false;
		return queryHasResults(deadline, session);
	}

	private boolean queryHasResults(SQLQuery q, Session session) {
		boolean result = false;
		boolean created = false;
		try {
			if (q == null)
				return false;
			
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				created = true;
			}
			List<Map<String, Integer>> results = session.createSQLQuery(q.toString())
					.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();

			result = results.size() > 0;
		} catch (Exception e) {
			if (session != null && session.getTransaction() != null)
				session.getTransaction().rollback();
			System.err.println("SQL QueryHasResults Error: " + e.getMessage());
		} finally {
			if (created)
				session.close();
		}

		return result;
	}

	public List<PolicyGoal> getGoal() {
		return this.goal;
	}

	@Override
	public String toString() {
		BasicFormatterImpl sqlFormatter = new BasicFormatterImpl();

		String ret = "\nPolicy [Name: " + name + ", Modality: " + modality + ", Addressee: " + addressee + "]";

		ret += "\n\n\tGoals: ";
		ret += goal.toString();

		ret += "\n\n\tExpiration Conditions: ";
		if (expiration != null)
			ret += "\t\t\t" + sqlFormatter.format(expiration.toString());

		ret += "\n\n\tDeadline Conditions: ";
		if (deadline != null)
			ret += sqlFormatter.format(deadline.toString());

		ret += "\n\tCost: " + String.valueOf(cost);
		ret += "\n-------------------------------------------------------";

		return ret;
	}

	// TODO write better equals and hashcode functions
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ActivePolicy that = (ActivePolicy) o;

		if (Objects.equals(name, that.name) && Objects.equals(modality, that.modality)) {
			if (bindings.keySet().equals(that.bindings.keySet())) {
				for (String k : bindings.keySet()) {
					if (!bindings.get(k).equals(that.bindings.get(k)))
						return false;
				}
				return true;
			} else {
				return false;
			}
		}

		/* TODO re-write this comparison */
		return false;

		/*
		 * 
		 * return Objects.equals(addressee, that.addressee) &&
		 * Objects.equals(modality, that.modality) && Objects.equals(name,
		 * that.name) && Objects.equals(expiration, that.expiration) &&
		 * Objects.equals(deadline, that.deadline) && Objects.equals(goal,
		 * that.goal) && Objects.equals(bindings, that.bindings);
		 * 
		 */
	}

	@Override
	public int hashCode() {
		String hash = name + addressee + modality;

		/* re-write hashcode */
		List<String> keys = new ArrayList<String>(bindings.keySet());
		List<String> values = new ArrayList<String>(bindings.values());

		Collections.sort(keys);
		Collections.sort(values);

		for (int i = 0; i < keys.size(); i++) {
			hash += keys.get(i) + values.get(i);
		}

		return Objects.hash(hash);
		// return Objects.hash(addressee, modality, name, cost,
		// expirationSysFunctions, deadline, goal, bindings, expirationQueries);
	}
}

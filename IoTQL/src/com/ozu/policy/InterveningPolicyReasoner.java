package com.ozu.policy;

import org.hibernate.Session;
import com.ozu.parser.*;

import java.util.*;

/**
 * Created by xx on 18/05/15.
 */
public class InterveningPolicyReasoner extends PolicyReasoner {

    private Session session;

    public InterveningPolicyReasoner(Session session) {
        super();
        this.session = session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void updateNormativeState() {
        checkExpirationConditions(session);
        checkActivationConditions(session);
    }

    public void checkActivationConditions(Session session) {
        for (Policy policy : policies) {
            HashSet<ActivePolicy> result = policy.executeActivationConditions(session);
            List<PolicyGoal> goals = policy.getGoal();

            if (!result.isEmpty()) {
                if (policy.getModality().equals("owlpolar:'Prohibition'")) {
                    for (PolicyGoal goal : goals)
                        normativeState.addProhibition(goal.getAction(), result);
                } else if (policy.getModality().equals("owlpolar:'Obligation'")) {
                    normativeState.addObligation(result);
                }
            }
        }
    }

    public void checkExpirationConditions(Session session) {
        for (String key : normativeState.getProhibitions().keySet()) {
            HashSet<ActivePolicy> curr = normativeState.getProhibitions().get(key);
            for (Iterator<ActivePolicy> i = curr.iterator(); i.hasNext(); ) {
                if (i.next().isExpired(session))
                    i.remove();
            }
        }

        /* TODO Can obligations expire without executing the action? */
        for (Iterator<ActivePolicy> i = normativeState.getObligations().iterator(); i.hasNext(); ) {
            if (i.next().isExpired(session))
                i.remove();
        }
    }

    /* TODO Check obligations with their deadline bindings */
    public double checkPolicyViolations(LispExprList operator) {
        String action = operator.get(0).toString().replace("!", "");
        HashSet<String> violations = new HashSet<String>();

        double cost = 0.0;

        if (normativeState.getProhibitions().containsKey(action)) {
            List<String> bindings = new ArrayList<String>();
            for (int i = 1; i < operator.size(); i++)
                bindings.add(operator.get(i).toString());
//                bindings.add(ONTOLOGY_IRI + operator.get(i).toString());

            PolicyGoal goal = new PolicyGoal(action, bindings);
            for (ActivePolicy ap : normativeState.getProhibitions().get(action)) {
                if (!violations.contains(ap.getName()) && ap.checkViolation(goal, session)) {
                    cost += ap.getCost();
                    violations.add(ap.getName());
                }
            }

            for (Iterator<ActivePolicy> i = normativeState.getObligations().iterator(); i.hasNext(); ) {
            	ActivePolicy curr = i.next();
                if (!violations.contains(curr.getName()) && curr.checkViolation(goal, session)) {
                    cost += curr.getCost();
                    violations.add(curr.getName());
                    i.remove();
                }
            }
        }

        return cost;
    }
}

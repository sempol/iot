package com.ozu.policy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by xx on 17/05/15.
 */
public class NormativeState {

	/* Obligations might be shared with a device manager that executes them */
    private PriorityBlockingQueue<ActivePolicy> obligations;
    
    private HashMap<String, HashSet<ActivePolicy>> prohibitions;

    public PriorityBlockingQueue<ActivePolicy> getObligations() {
        return obligations;
    }

    public HashMap<String, HashSet<ActivePolicy>> getProhibitions() {
        return prohibitions;
    }

    public NormativeState() {
        prohibitions = new HashMap<String, HashSet<ActivePolicy>>();
        obligations = new PriorityBlockingQueue<ActivePolicy>(30, new ActivePolicyComparator());
    }

    public NormativeState(HashMap<String, HashSet<ActivePolicy>> prohibitions, PriorityBlockingQueue<ActivePolicy> obligations) {
        this.prohibitions = prohibitions;
        this.obligations = obligations;
    }

    public NormativeState cloneState() {
        HashMap<String, HashSet<ActivePolicy>> cloneRest = new HashMap<String, HashSet<ActivePolicy>>();
        for (String k : prohibitions.keySet()) {
            HashSet<ActivePolicy> set = new HashSet<ActivePolicy>();
            HashSet<ActivePolicy> currSet = prohibitions.get(k);

            for (ActivePolicy ap : currSet)
                set.add(ap.cloneActivePolicy());

            cloneRest.put(k, set);
        }

        PriorityBlockingQueue<ActivePolicy> cloneOb = new PriorityBlockingQueue<ActivePolicy>();
        for (Iterator<ActivePolicy> i = obligations.iterator(); i.hasNext(); )
            cloneOb.add(i.next().cloneActivePolicy());

        return new NormativeState(cloneRest, cloneOb);
    }

    public void addProhibition(String key, HashSet<ActivePolicy> value) {
        if (prohibitions.containsKey(key))
            prohibitions.get(key).addAll(value);
        else
            prohibitions.put(key, new HashSet<ActivePolicy>(value));
    }

    public void addObligation(HashSet<ActivePolicy> value) {
        obligations.addAll(value);
    }
}

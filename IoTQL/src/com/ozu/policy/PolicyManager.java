package com.ozu.policy;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.hp.hpl.jena.query.Query;
import com.ozu.owlql.QLQueryRewriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.ozu.utils.StringConstants.POLICY_XML_PATH;

/**
 * Created by xx on 18/05/15.
 */
public class PolicyManager {
	private Set<Policy> policies;
	private QLQueryRewriter qr;

	private static final String REGEX = ",(?=(?:[^']*'[^']*')*[^']*$)(?=(?:[^()]*\\([^()]*\\))*[^()]*$)";

	private static PolicyManager instance = null;

	private PolicyManager() {
		policies = new HashSet<Policy>();
		qr = QLQueryRewriter.getInstance();
	}

	public static PolicyManager getInstance() {
		if (instance == null) {
			// Thread Safe. Might be costly operation in some case
			synchronized (PolicyManager.class) {
				if (instance == null)
					instance = new PolicyManager();
			}
		}
		return instance;
	}

	private List<PolicyGoal> parseGoals(String goalText) {
		return PolicyGoal.policyGoalfromStringSet(new HashSet<String>(Arrays.asList(goalText.split(REGEX))));
	}

	public boolean readPolicies() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(POLICY_XML_PATH));

		NodeList nodeList = document.getDocumentElement().getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equals("Policy")) {

				String name = node.getAttributes().getNamedItem("Name").getNodeValue();
				String modality = node.getAttributes().getNamedItem("Modality").getNodeValue();
				String addressee = node.getAttributes().getNamedItem("Addressee").getNodeValue();

				double cost = 0;

				Query addresseeRole = null;
				Query activation = null;
				Query expiration = null;
				Query deadline = null;
				Query actionDescription = null;

				Set<String> activationSystemFunctions = new HashSet<String>();
				Set<String> expirationSystemFunctions = new HashSet<String>();
				Set<String> deadlineSystemFunctions = new HashSet<String>();

				List<PolicyGoal> goal = null;

				String oRole = "";
				List<String> oActivation = null;

				String actionVar = "";
				List<String> oActionDescription = null;

				NodeList childList = node.getChildNodes();
				for (int j = 0; j < childList.getLength(); j++) {
					Node childNode = childList.item(j);

					String label = childNode.getNodeName().replace(" ", "");
					String text = childNode.getTextContent().replace(" ", "");

					switch (label) {
					case "AddresseeRole":
						oRole = text;
						addresseeRole = qr.rewriteCondition(text);
						break;
					case "Activation":
						text = oRole += ", " + text; // add the addressee role
						oActivation = Arrays.asList(text.split(REGEX));
						activation = qr.rewriteConditions(text.split(REGEX), activationSystemFunctions);
						break;
					case "Goal":
						goal = parseGoals(text);
						break;
					case "ActionDescription":
						actionVar = childNode.getAttributes().getNamedItem("Var").toString();
						oActionDescription = Arrays.asList(text.split(REGEX));
						actionDescription = qr.rewriteConditions(text.split(REGEX), null);
						break;
					case "Expiration":
						text = oRole += ", " + text; // add the addressee role
						expiration = qr.rewriteConditions(text.split(REGEX), expirationSystemFunctions);
						break;
					case "Deadline":
						deadline = qr.rewriteConditions(text.split(REGEX), deadlineSystemFunctions);
						break;
					case "Cost":
						cost = Double.parseDouble(text);
						break;
					default:
						break;
					}
				}

				Policy policy = new Policy(addressee, addresseeRole, modality, name, cost, activation,
						activationSystemFunctions, expiration, expirationSystemFunctions, deadline,
						deadlineSystemFunctions, goal);

				policy.setOriginalAddresseeRole(oRole);
				policy.setOriginalActivation(oActivation);

				policy.setActionVar(actionVar);
				policy.setOriginalActionDescription(oActionDescription);
				policy.setActionDescription(actionDescription);

				policies.add(policy);
			}
		}

		return true;
	}

	public Set<Policy> getPolicies(String key) {
		/* TODO return ONLY a copy of required policies */
		return policies;
	}

	public boolean checkPolicyConsistency() {
		return false;
	}

}

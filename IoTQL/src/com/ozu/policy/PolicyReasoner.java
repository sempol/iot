package com.ozu.policy;

import java.io.IOException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.xml.sax.SAXException;

import com.ozu.database.hibernate.SandboxUtil;
import com.ozu.knowledgebase.devices.DeviceObligationObserver;
import com.ozu.ont.pojo.boilerplate.impl.DataProperty;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.EDataType;
import com.ozu.owlql.QLReasoner;
import com.ozu.owlql.ReasonerFactory;

/**
 * Created by xx on 07/04/15.
 */
public class PolicyReasoner {
	/* TODO group active policies by name too */
	protected NormativeState normativeState;
	protected Set<Policy> policies;

	public PolicyReasoner() {
		normativeState = new NormativeState();
		policies = PolicyManager.getInstance().getPolicies("");
	}

	public void updateNormativeState() {
		checkExpirationConditions();
		checkActivationConditions();
	}

	public void checkActivationConditions() {
		for (Policy policy : policies) {
			HashSet<ActivePolicy> result = policy.executeActivationConditions();
			List<PolicyGoal> goals = policy.getGoal();

			if (!result.isEmpty()) {
				if (policy.getModality().equals("owlpolar:'Prohibition'"))
					for (PolicyGoal goal : goals)
						normativeState.addProhibition(goal.getAction(), result);
				else if (policy.getModality().equals("owlpolar:'Obligation'"))
					normativeState.addObligation(result);
			}
		}
	}

	public void checkExpirationConditions() {
		for (String key : normativeState.getProhibitions().keySet()) {
			HashSet<ActivePolicy> curr = normativeState.getProhibitions().get(key);

			for (Iterator<ActivePolicy> i = curr.iterator(); i.hasNext();) {
				if (i.next().isExpired())
					i.remove();
			}
		}

		/* TODO Can obligations expire without executing the action? */
		for (Iterator<ActivePolicy> i = normativeState.getObligations().iterator(); i.hasNext();) {
			if (i.next().isExpired())
				i.remove();
		}
	}

	private int getOrCreateIndividual(String var, Map<String, Integer> cache, int cnt, Session session) {
		if (cache.containsKey(var))
			return cache.get(var);

		int id = Thing.createIndividualIfNotExists("ind" + cnt, session);
		cache.put(var, id);
		return id;
	}

	// returns number of individuals
	private int freezeQuery(List<String> conditions, Map<String, Integer> cache, int indCnt, Session session) {
		for (String c : conditions) {
			if (c.contains("sspn:")) {
				String pred = c.substring(c.indexOf("'") + 1, c.lastIndexOf("'"));
				String[] params = c.substring(c.indexOf("(") + 1, c.indexOf(")")).split(",");

				int ind = getOrCreateIndividual(params[0], cache, indCnt++, session);

				if (params.length == 1) {
					Thing.addIndividualToSession(ind, pred, session);
				} else if (params.length == 2) {
					if (params[1].charAt(0) == '?') {
						int ind2 = getOrCreateIndividual(params[1], cache, indCnt++, session);
						ObjectProperty.addObjectPropertyToSession(pred, ind, ind2, session);
					} else {
						/* TODO this has to be tested */
						/* Currently all of them are inserted as strings */
						DataProperty.addDataPropertyToSession(ind, pred, params[1], EDataType.STRING, session);
					}
				}
			} else if (c.contains("sys:")) {

			}
		}

		return indCnt;
	}

	private boolean checkModalityConflict(Policy a, Policy b) {
		/* Goal based policies also make modality check easier */
		if ((a.getModality().contains("Obligation") && b.getModality().contains("Prohibition"))
				|| (b.getModality().contains("Obligation") && a.getModality().contains("Prohibition"))) {
			return true;
		}

		return false;
	}

	public boolean checkConflict(Policy pA, Policy pB) {
		/*
		 * TODO proper modality check and check user role first for
		 * optimization?
		 * 
		 * - check if addressee roles are disjoint
		 */
		if (!checkModalityConflict(pA, pB))
			return false;

		Session session = SandboxUtil.getSessionFactory().openSession();

		int indCnt = 0;

		HashMap<String, Integer> varIndMap = new HashMap<String, Integer>();

		Policy p1 = null;
		Policy p2 = null;

		List<Map<String, Integer>> results = null;
		for (int i = 0; i < 2; i++) {
			p1 = i == 0 ? pA : pB;
			p2 = i == 0 ? pB : pA;

			if (i == 1) {
				session.close();
				session = SandboxUtil.getSessionFactory().openSession();
			}

			indCnt = freezeQuery(p1.getOriginalActivation(), varIndMap, indCnt, session);
			indCnt = freezeQuery(p1.getOriginalActionDescription(), varIndMap, indCnt, session);

			try {
				results = session.createSQLQuery(p2.getActionDescription().toString())
						.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();

			} catch (Exception e) {
				if (session != null && session.getTransaction() != null)
					session.close();
				System.err.println("PolicyReasoner Conflict Detection SQL Error: " + e.getMessage());
				return false;
			}

			if (!results.isEmpty())
				break;
		}

		if (results == null || results.isEmpty())
			return false;

		int addressee = varIndMap.get(p1.getAddressee());
		for (Map<String, Integer> result : results) {
			if (result.containsKey(p2.getAddressee()) && result.get(p2.getAddressee()) != addressee)
				continue;

			result.put(p2.getAddressee(), addressee);

			indCnt = freezeQuery(p2.getOriginalActivation(), result, indCnt, session);

			// maybe more than one instance are created?
			HashSet<ActivePolicy> activeA = p1.executeActivationConditions(session);
			HashSet<ActivePolicy> activeB = p2.executeActivationConditions(session);

			// check if policies expire
			for (Iterator<ActivePolicy> curr = activeA.iterator(); curr.hasNext();) {
				ActivePolicy p = curr.next();
				/* Check deadline too? */
				if (p.isExpired(session))
					curr.remove();
			}

			for (Iterator<ActivePolicy> curr = activeB.iterator(); curr.hasNext();) {
				ActivePolicy p = curr.next();
				/* Check deadline too? */
				if (p.isExpired(session))
					curr.remove();
			}

			/* Does not conflict */
			if (activeA.size() == 0 || activeB.size() == 0)
				continue;

			// check if the state is consistent. if it is, there is a conflict.
			QLReasoner qlReasoner = new ReasonerFactory().getReasonerWithSession(session);
			if (qlReasoner.isConsistent()) {
				session.close();
				return true;
			}

			for (String key : result.keySet())
				Thing.deleteIndividualFromSession(result.get(key), session);
		}
		session.close();
		return false;
	}

	public void checkPolicyConflicts() {
		/* TODO: Bad implementation - This is just to test */
		for (Iterator<Policy> curr = policies.iterator(); curr.hasNext();) {
			Policy a = curr.next();
			curr.remove();
			for (Policy b : policies)
				checkConflict(a, b);
		}
	}

	public DeviceObligationObserver getObligationObserver() throws IOException {
		return new DeviceObligationObserver(normativeState.getObligations());
	}

	public NormativeState cloneNormativeState() {
		return normativeState.cloneState();
	}

	public Set<Policy> getAllPolicies() {
		return policies;
	}

	public static void main(String args[]) throws ParserConfigurationException, IOException, SAXException {
		PolicyManager.getInstance().readPolicies();
		// OWLQLCompiler compiler = QLQueryRewriter.getInstance().getCompiler();
		// ConsistencyCheckResult computeConsistencyCheck =
		// compiler.computeConsistencyCheck();
		//
		// Session session = HibernateUtil.getSessionFactory().openSession();
		//
		// String q = new
		// QueryTranslator().convertSPARQL2SQL(computeConsistencyCheck.getInconsistencyDetectionAskQuery())
		// .toString();
		// List results = session.createSQLQuery(q).list();
		//
		// session.close();
		// PolicyReasoner policyReasoner = new PolicyReasoner();
		// policyReasoner.updateNormativeState();
		// policyReasoner.checkPolicyConflicts();
	}
}

package com.ozu.ql.database;

/**
 * Created by xx on 09/06/15.
 */

import java.util.ArrayList;
import java.util.List;

import com.ozu.ql.database.EPBQuery;
import com.ozu.ql.database.EUQuery;

public class EUQuery {
	private String var;
	private List<EUQuery> unions;
	private List<EPBQuery> pathblocks;

	public String getVar() {
		return var;
	}

	public List<EUQuery> getUnions() {
		return unions;
	}

	public List<EPBQuery> getPathBlocks() {
		return pathblocks;
	}

	public EUQuery(String var, List<EUQuery> eus, List<EPBQuery> epbs) {
		super();
		this.var = var;
		this.unions = eus;
		this.pathblocks = epbs;
	}

	public EUQuery cloneQuery() {
		List<EUQuery> _unions = new ArrayList<EUQuery>();
		for (EUQuery q : this.unions)
			_unions.add(q.cloneQuery());

		List<EPBQuery> _pathblocks = new ArrayList<EPBQuery>();
		for (EPBQuery q : this.pathblocks)
			_pathblocks.add(q.cloneQuery());

		return new EUQuery(var, _unions, _pathblocks);
	}

	public String toSQLQuery() {
		String sql = "(";

		boolean first = true;
		for (EPBQuery pb : pathblocks) {
			if (!first)
				sql += " UNION ";

			sql += pb.toSQLQuery();
			first = false;
		}

		for (EUQuery eu : unions) {
			if (!first)
				sql += " UNION ";

			sql += eu.toSQLQuery();
			first = false;
		}

		sql += ")";
		return sql;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pathblocks == null) ? 0 : pathblocks.hashCode());
		result = prime * result + ((unions == null) ? 0 : unions.hashCode());
		result = prime * result + ((var == null) ? 0 : var.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EUQuery other = (EUQuery) obj;
		if (pathblocks == null) {
			if (other.pathblocks != null)
				return false;
		} else if (!pathblocks.equals(other.pathblocks))
			return false;
		if (unions == null) {
			if (other.unions != null)
				return false;
		} else if (!unions.equals(other.unions))
			return false;
		if (var == null) {
			if (other.var != null)
				return false;
		} else if (!var.equals(other.var))
			return false;
		return true;
	}

}

package com.ozu.ql.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ozu.ql.database.EPBQuery;
import com.ozu.ql.database.EUQuery;

/**
 * Created by xx on 11/06/15.
 */
public class SQLQuery {
	private List<EPBQuery> epbQueries;
	private List<EUQuery> euQueries;

	private HashMap<String, String> varColMap;
	private HashMap<String, String> bindings;

	private boolean bindVariables;
	private boolean isAskQuery;

	public SQLQuery(List<EPBQuery> epbQueries, List<EUQuery> euQueries, HashMap<String, String> varColMap,
			boolean isAskQuery) {
		this.epbQueries = epbQueries;
		this.euQueries = euQueries;
		this.varColMap = varColMap;
		this.bindings = new HashMap<String, String>();
		this.isAskQuery = isAskQuery;
	}

	public SQLQuery(List<EPBQuery> epbQueries, List<EUQuery> euQueries, HashMap<String, String> varColMap,
			HashMap<String, String> bindings, boolean bindVariables) {
		this.epbQueries = epbQueries;
		this.euQueries = euQueries;
		this.varColMap = varColMap;
		this.bindings = bindings;
		this.bindVariables = bindVariables;
	}

	public void setBindings(HashMap<String, String> bindings) {
		this.bindings = new HashMap<String, String>(bindings);
		this.bindVariables = true;
	}

	public void setBindVariables(boolean bindVariables) {
		this.bindVariables = bindVariables;
	}

	public SQLQuery cloneQuery() {
		List<EUQuery> _unions = new ArrayList<EUQuery>();
		for (EUQuery q : this.euQueries)
			_unions.add(q.cloneQuery());

		List<EPBQuery> _pathblocks = new ArrayList<EPBQuery>();
		for (EPBQuery q : this.epbQueries)
			_pathblocks.add(q.cloneQuery());

		HashMap<String, String> _varColMap = new HashMap<String, String>(this.varColMap);
		HashMap<String, String> _bindings = new HashMap<String, String>(this.bindings);

		return new SQLQuery(_pathblocks, _unions, _varColMap, _bindings, this.bindVariables);
	}

	@Override
	public String toString() {
		/* TODO Distinct? */
		String joins = "";
		String finalQueryString = "";

		boolean first = true;

		EPBQuery finalQuery = new EPBQuery();
		for (EPBQuery epbq : epbQueries)
			finalQuery.mergeWith(epbq);

		boolean where = false;
		if (!epbQueries.isEmpty()) {
			if (euQueries.isEmpty())
				finalQueryString = finalQuery.toSQLQuery();
			else
				finalQueryString += " (" + finalQuery.toSQLQuery() + ") AS finalquery ";

			first = false;
			where = finalQuery.getWhere();

			/* TODO this is required for filter expressions */
			for (String key : finalQuery.getSelectVariables().keySet()) {
				if (!varColMap.containsKey(key)) {
					if (!euQueries.isEmpty())
						varColMap.put(key, "finalquery");
					else
						varColMap.put(key, finalQuery.getSelectVariables().get(key).iterator().next());
				}
			}
		}

		int cnt = 0;
		for (EUQuery eu : euQueries) {
			String qname = "union" + (char) (cnt + 'a');

			if (!epbQueries.isEmpty() && finalQuery.containsVar(eu.getVar())) {

				if (!first)
					joins += " INNER JOIN ";

				joins += eu.toSQLQuery() + " AS " + qname;

				if (!epbQueries.isEmpty() && finalQuery.containsVar(eu.getVar()))
					joins += " ON finalquery." + eu.getVar() + "=" + qname + "." + eu.getVar();

			} else {
				if (!first)
					finalQueryString = eu.toSQLQuery() + " AS " + qname + ", " + finalQueryString;
				else
					finalQueryString = eu.toSQLQuery() + " AS " + qname;
			}

			if (!varColMap.containsKey(eu.getVar()))
				varColMap.put(eu.getVar(), qname);

			first = false;
			cnt++;
		}

		String sql = finalQueryString;
		if (!euQueries.isEmpty()) {
			/* Set Column Aliases */
			String select = "SELECT ";
			first = true;
			for (String var : varColMap.keySet()) {
				if (!first)
					select += ", ";

				select += varColMap.get(var) + "." + var + " AS " + var;

				first = false;
			}

			if (isAskQuery)
				select = "SELECT 1";

			sql = select + " FROM " + finalQueryString + joins;
		}

		if (bindVariables && (bindings != null || bindings.size() > 0)) {
			if (where) {
				for (String key : bindings.keySet()) {
					String val = varColMap.get(key.substring(1));
					if (val != null && val.length() > 0)
						sql += " AND " + val + "." + key.substring(1) + "=" + bindings.get(key);
				}
			} else {
				first = true;
				for (String key : bindings.keySet()) {
					String val = varColMap.get(key.substring(1));
					if (val != null && val.length() > 0) {
						if (!first)
							sql += " AND ";
						else
							sql += " WHERE ";

						sql += val + "." + key.substring(1) + "=" + bindings.get(key);
						first = false;
					}
				}
			}
		} else if (!euQueries.isEmpty()) {
			first = true;
			for (String var : varColMap.keySet()) {
				if (!first)
					sql += " AND ";
				else {
					if (isAskQuery)
						break;
					sql += " WHERE ";
				}

				sql += varColMap.get(var) + "." + var + " IS NOT NULL";
				first = false;
			}
		}

		return sql;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bindVariables ? 1231 : 1237);
		result = prime * result + ((bindings == null) ? 0 : bindings.hashCode());
		result = prime * result + ((epbQueries == null) ? 0 : epbQueries.hashCode());
		result = prime * result + ((euQueries == null) ? 0 : euQueries.hashCode());
		result = prime * result + ((varColMap == null) ? 0 : varColMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SQLQuery other = (SQLQuery) obj;
		if (bindVariables != other.bindVariables)
			return false;
		if (bindings == null) {
			if (other.bindings != null)
				return false;
		} else if (!bindings.equals(other.bindings))
			return false;
		if (epbQueries == null) {
			if (other.epbQueries != null)
				return false;
		} else if (!epbQueries.equals(other.epbQueries))
			return false;
		if (euQueries == null) {
			if (other.euQueries != null)
				return false;
		} else if (!euQueries.equals(other.euQueries))
			return false;
		if (varColMap == null) {
			if (other.varColMap != null)
				return false;
		} else if (!varColMap.equals(other.varColMap))
			return false;
		return true;
	}

}

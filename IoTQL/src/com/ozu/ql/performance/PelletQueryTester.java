package com.ozu.ql.performance;

import org.mindswap.pellet.jena.PelletReasonerFactory;


import com.clarkparsia.pellet.sparqldl.jena.SparqlDLExecutionFactory;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

public class PelletQueryTester {
	private OntModel model;
	private ArrayList<Query> queries;

	public PelletQueryTester() throws MalformedURLException {
		model = ModelFactory.createOntologyModel(PelletReasonerFactory.THE_SPEC);
		model.read(Paths.get("./benchmark/benchmark.owl").toFile().toURI().toURL().toString());
		model.prepare();
	}

	public Query query(String queryStr) {
		return QueryFactory.create(queryStr);
	}

	public void setQueries(ArrayList<String> queryStrings) {
		queries = new ArrayList<Query>();
		for (String q : queryStrings) {
			queries.add(query(q));
		}
	}

	public void executeQueries() {
		Instant start = Instant.now();
		for (Query q : queries) {
			executeQuery(q);
		}
		Instant end = Instant.now();
		System.out.println("Pellet Query Execution: " + Duration.between(start, end).getNano() + " seconds"); // prints PT1M3.553S
	}

	public void executeQuery(Query q) {
		QueryExecution qe = SparqlDLExecutionFactory.create(q, model);
		ResultSet rs = qe.execSelect();
	}
}

package com.ozu.ql.performance;

import static com.ozu.utils.StringConstants.ONTOLOGY_PATH_RDF;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.ibm.research.owlql.ConjunctiveQuery;
import com.ibm.research.owlql.OWLQLCompiler;
import com.ibm.research.owlql.ruleref.OWLQLToNonRecursiveDatalogCompiler;
import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.owlql.QLQueryRewriter;
import com.ozu.ql.database.QueryTranslator;
import com.ozu.ql.database.SQLQuery;

public class QLQueryTester {
	private OWLQLToNonRecursiveDatalogCompiler datalogCompiler;
	private OWLOntology qlOnt;
	private OWLOntologyManager manager;
	private ArrayList<SQLQuery> queries;

	public QLQueryTester() {

	}

	public void init() {
		Instant start = Instant.now();
		manager = OWLManager.createOWLOntologyManager();
		try {
			qlOnt = manager.loadOntologyFromOntologyDocument(new File("./benchmark/benchmark.owl"));
		} catch (OWLOntologyCreationException e) {
			System.err.println("Could not load the ontology." + e);
			System.exit(-1);
		} // .createOntology();
		datalogCompiler = new OWLQLToNonRecursiveDatalogCompiler(qlOnt, null, null);
		Instant end = Instant.now();
		System.out.println("QL Initialization: " + Duration.between(start, end).getSeconds() + " seconds"); // prints
																											// PT1M3.553S
	}

	public ArrayList<SQLQuery> setQueries(ArrayList<String> queryStrings) throws IOException {
		Instant start = Instant.now();
		QueryTranslator translator = new QueryTranslator();
		queries = new ArrayList<SQLQuery>();
		for (String query : queryStrings) {
			ConjunctiveQuery conjunctiveQuery = new ConjunctiveQuery(QueryFactory.create(query));
			Query q = datalogCompiler.compileToUnionQuery(conjunctiveQuery);
			queries.add(translator.convertSPARQL2SQL(q));
		}
		System.out.println(queries);
		Instant end = Instant.now();
		System.out.println("QL Reading and Translating Queries From Sparql: "
				+ Duration.between(start, end).getSeconds() + " seconds"); // prints
																			// PT1M3.553S
		return queries;
	}

	public void executeQueries() {
		Instant start = Instant.now();
		Session session = HibernateUtil.getSessionFactory().openSession();
		for (SQLQuery query : queries) {
			List<Map<String, Integer>> results = session.createSQLQuery(query.toString())
					.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();
		}
		session.close();
		Instant end = Instant.now();
		System.out.println("QL Query Execution: " + Duration.between(start, end).getSeconds() + " seconds"); // prints
		System.out.println("QL Query Execution: " + Duration.between(start, end).getNano() + " seconds"); // prints
	}

	public void executeSQLQuery(String sql) {
		Instant start = Instant.now();
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Map<String, Integer>> results = session.createSQLQuery(sql)
				.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE).list();
		System.out.println("Result Size: " + results.size());
		session.close();
		Instant end = Instant.now();
		System.out.println("QL Query Execution: " + Duration.between(start, end).getSeconds() + " seconds"); // prints
		System.out.println("QL Query Execution: " + Duration.between(start, end).getNano() + " seconds"); // prints
	}
}

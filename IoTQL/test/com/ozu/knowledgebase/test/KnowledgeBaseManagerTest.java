package com.ozu.knowledgebase.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.gson.GsonBuilder;
import com.ozu.hypercat.json.deserializers.CatalogueDeserializer;
import com.ozu.hypercat.objects.HypercatCatalogue;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.Senml;
import com.ozu.knowledgebase.KnowledgeBaseManager;
import com.ozu.knowledgebase.hypercat.commands.NewInformation;
import com.ozu.knowledgebase.hypercat.commands.SenmlCommand;
import com.ozu.knowledgebase.hypercat.commands.UpdateInformation;
import com.ozu.owlql.InconsistentKBException;

public class KnowledgeBaseManagerTest {

	public String readFile(String name) {
		try {
			return new String(Files.readAllBytes(Paths.get(name)));
		} catch (IOException e) {
			System.out.println("Could not read file: " + name);
			return "";
		}
	}

	@Test
	public void readFiles() {
		String json = readFile("utils/catalog_with_catalogues.json");

		assertNotNull(json);
		assertNotEquals(0, json.length());
	}

	@Ignore("not ready yet")
	@Test
	public void newCatalogueWithCataloguesTest() {
		String json = readFile("utils/catalog_with_catalogues.json");

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(HypercatCatalogue.class,
				new CatalogueDeserializer());

		HypercatCatalogue newCatalogue = (gsonBuilder.create()).fromJson(json,
				HypercatCatalogue.class);

		try {
			KnowledgeBaseManager.getInstance().processHypercatCommand(
					new NewInformation(newCatalogue));
		} catch (ParserConfigurationException | IOException | SAXException
				| InconsistentKBException e) {
			fail("Test failed because of an exception in KB: " + e.getMessage());
			e.printStackTrace();
		}
	}

//	@Ignore("not ready yet")
	@Ignore("not ready yet")
	@Test
	public void newSensorCatalogueTest() {
		String json = readFile("utils/sensor_cat.json");

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(HypercatCatalogue.class,
				new CatalogueDeserializer());

		HypercatCatalogue newCatalogue = (gsonBuilder.create()).fromJson(json,
				HypercatCatalogue.class);

		try {
			KnowledgeBaseManager.getInstance().processHypercatCommand(
					new NewInformation(newCatalogue));
		} catch (ParserConfigurationException | IOException | SAXException
				| InconsistentKBException e) {
			fail("Test failed because of an exception in KB: " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Ignore("not ready yet")
	@Test
	public void updateItemTest() {
		String json = readFile("utils/item.json");

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(HypercatCatalogue.class,
				new CatalogueDeserializer());

		HypercatItem oldItem = (gsonBuilder.create()).fromJson(json,
				HypercatItem.class);

		json = readFile("utils/update_item.json");

		gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(HypercatCatalogue.class,
				new CatalogueDeserializer());

		HypercatItem newItem= (gsonBuilder.create()).fromJson(json,
				HypercatItem.class);

		try {
			KnowledgeBaseManager.getInstance().processHypercatCommand(
					new UpdateInformation(newItem, oldItem));
		} catch (ParserConfigurationException | IOException | SAXException
				| InconsistentKBException e) {
			fail("Test failed because of an exception in KB: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Test
	public void processSenmlFile() {
		String json = readFile("utils/TemperatureSensorEmreRoomOutput.senml");

		GsonBuilder gsonBuilder = new GsonBuilder();
		Senml item = (gsonBuilder.create()).fromJson(json,
				Senml.class);

		try {
			KnowledgeBaseManager.getInstance().updateFromSenmlFile(new SenmlCommand(item, 20));
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.println("Enter something to finish");
	        String s = br.readLine();
		} catch (ParserConfigurationException | IOException | SAXException
				| InconsistentKBException e) {
			fail("Test failed because of an exception in KB: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Ignore("not ready yet")
	@Test
	public void processUpdatedSenmlFile() {
		String json = readFile("utils/UpdatedTemp.senml");

		GsonBuilder gsonBuilder = new GsonBuilder();
		
		Senml newItem = (gsonBuilder.create()).fromJson(json,
				Senml.class);

		try {
			KnowledgeBaseManager.getInstance().updateFromSenmlFile(new SenmlCommand(newItem, 20));
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.println("Enter something to finish");
	        String s = br.readLine();
		} catch (ParserConfigurationException | IOException | SAXException
				| InconsistentKBException e) {
			fail("Test failed because of an exception in KB: " + e.getMessage());
			e.printStackTrace();
		}
	}

}

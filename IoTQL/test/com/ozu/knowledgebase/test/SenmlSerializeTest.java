package com.ozu.knowledgebase.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.google.gson.GsonBuilder;
import com.ozu.hypercat.objects.Senml;

public class SenmlSerializeTest {

	@Test
	public void singleDataPoint() {
		String json = "{\"e\":[{ \"n\": \"urn:dev:ow:10e2073a01080063\", \"v\":23.5 }]}";
		GsonBuilder gsonBuilder = new GsonBuilder();

		Senml output = (gsonBuilder.create()).fromJson(json, Senml.class);
		System.out.println(output);
		assertNotNull(output);
	}

	@Test
	public void multipleDataPoints() {
		String json = "{\"e\":[{ \"n\": \"voltage\", \"t\": 0, \"u\": \"V\", \"v\": 120.1 },{ \"n\": \"current\", \"t\": 0, \"u\": \"A\", \"v\": 1.2 }],\"bn\": \"urn:dev:mac:0024befffe804ff1/\"}";
		GsonBuilder gsonBuilder = new GsonBuilder();

		Senml output = (gsonBuilder.create()).fromJson(json, Senml.class);
		System.out.println(output);
		assertNotNull(output);
		
		json = "{\"e\":[{ \"n\": \"voltage\", \"u\": \"V\", \"v\": 120.1 },{ \"n\": \"current\", \"t\": -5, \"v\": 1.2 },{ \"n\": \"current\", \"t\": -4, \"v\": 1.30 },{ \"n\": \"current\", \"t\": -3, \"v\": 0.14e1 },{ \"n\": \"current\", \"t\": -2, \"v\": 1.5 },{ \"n\": \"current\", \"t\": -1, \"v\": 1.6 },{ \"n\": \"current\", \"t\": 0,   \"v\": 1.7 }],\"bn\": \"urn:dev:mac:0024befffe804ff1/\",\"bt\": 1276020076,\"ver\": 1,\"bu\": \"A\"}";
		output = (gsonBuilder.create()).fromJson(json, Senml.class);
		System.out.println(output);
		assertNotNull(output);
	}
	
	@Test
	public void multipleMeasurements() {
		String json = "{\"e\":[{ \"v\": 20.0, \"t\": 0 },{ \"sv\": \"E 24' 30.621\", \"u\":\"lon\", \"t\": 0 },{ \"sv\": \"N 60' 7.965\", \"u\": \"lat\", \"t\": 0 },{ \"v\": 20.3, \"t\": 60 },{ \"sv\": \"E 24' 30.622\", \"u\": \"lon\", \"t\": 60 },{ \"sv\": \"N 60' 7.965\", \"u\": \"lat\", \"t\": 60 },{ \"v\": 20.7, \"t\": 120 },{ \"sv\": \"E 24' 30.623\", \"u\": \"lon\", \"t\": 120 },{ \"sv\": \"N 60' 7.966\", \"u\": \"lat\", \"t\": 120 },{ \"v\": 98.0, \"u\": \"%EL\", \"t\": 150 },{ \"v\": 21.2, \"t\": 180 },{ \"sv\": \"E 24' 30.628\", \"u\": \"lon\", \"t\": 180 },{ \"sv\": \"N 60' 7.967\", \"u\": \"lat\", \"t\": 180 }],\"bn\": \"http://[2001:db8::1]\",\"bt\": 1320067464,\"bu\": \"%RH\"}";
		GsonBuilder gsonBuilder = new GsonBuilder();

		Senml output = (gsonBuilder.create()).fromJson(json, Senml.class);
		System.out.println(output);
		assertNotNull(output);
	}
}

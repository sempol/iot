package com.ozu.hypercat.demo;

import java.io.File;

import com.github.kevinsawicki.http.HttpRequest;

public class CreateInitialState {

	public static void main(String[] args) {

		int response = HttpRequest.post("http://localhost:4567/cats/flat1").contentType("application/json")
				.send(new File("demo/Flat1")).code();

		System.out.println(response);

		response = HttpRequest.post("http://localhost:4567/cats?href=http://localhost:4567/locate-people-in-flat")
				.contentType("application/json").send(new File("demo/LocationService")).code();

		System.out.println(response);

		response = HttpRequest.post("http://localhost:4567/cats/flat1/room1").contentType("application/json")
				.send(new File("demo/Room1")).code();

		System.out.println(response);

		response = HttpRequest.post("http://localhost:4567/cats/flat1/room1/speaker").contentType("application/json")
				.send(new File("demo/Speaker")).code();

		System.out.println(response);

		response = HttpRequest.post("http://localhost:4567/cats/flat1/room1/tv").contentType("application/json")
				.send(new File("demo/TV")).code();

		System.out.println(response);

		response = HttpRequest.post("http://localhost:4567/cats/flat1/room1/temperaturesensor")
				.contentType("application/json").send(new File("demo/TemperatureSensor")).code();

		System.out.println(response);

		/* This part is moved to Android Demo */
		response = HttpRequest.post("http://localhost:4567/cats/flat1/Doorbell").contentType("application/json")
				.send(new File("demo/Doorbell")).code();

		System.out.println(response);

	}

}

package com.ozu.hypercat.demo;

import static com.ozu.hypercat.ontology.OntologyConstants.LAST_EVENT_DATE_TIME;
import static com.ozu.hypercat.ontology.OntologyConstants.OUTPUT_TYPE;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.GsonBuilder;
import com.ozu.hypercat.objects.HypercatItem;

public class Doorbell extends Thread {
	private final static String PLAIN_IRI = "http://cs.ozyegin.edu.tr/xx/2014/12/sspn";
	private final static String OUTPUT_IRI = PLAIN_IRI + "#DoorbellOutput";
	private final static String CLASS_IRI = PLAIN_IRI + "#Doorbell";
	private final static String ON_PLATFORM = "http://purl.oclc.org/NET/ssnx/ssn#onPlatform";

	private String lastEvent;
	private String platform; // e.g. on front door
	private String individualName;
	
	private HttpClient client = HttpClientBuilder.create().build();
	
	public Doorbell (String _platform, String _name){
		platform = _platform.replace(" ", "");
		individualName = _name.replace(" ", "");
	}
	
	public String getIndividualName() {
		return individualName;
	}

	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}

	public void ringTheDoor () {
		lastEvent =  getTimeStamp();
	}

	public String getLastEvent() {
		return lastEvent;
	}

	public void setLastEvent(String lastEvent) {
		this.lastEvent = lastEvent;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public HypercatItem registerToServer() {
		// Don't send any kind of information related to "Sensing" in the register phase. e.g. lasteventdate
		HypercatItem doorbell = new HypercatItem(individualName, PLAIN_IRI);
		doorbell.addMetadata(CLASS_IRI, PLAIN_IRI + "#" + individualName);
		doorbell.addMetadata(ON_PLATFORM, PLAIN_IRI + "#" + platform);
		
		try {
			postRequest("http://localhost:4567/cats/root?href="+individualName,new StringEntity(new GsonBuilder().create().toJson(doorbell)));
		} catch (UnsupportedEncodingException e) {
			System.out.println ("UnsupportedEncodingException in Doorbell.registerToServer: " + e);
		}
		
		return doorbell;
	}
	
	public HypercatItem sendSignal() {
		HypercatItem doorbell = new HypercatItem(individualName, PLAIN_IRI);
		doorbell.addMetadata(CLASS_IRI, PLAIN_IRI + "#" + individualName);
		doorbell.addMetadata(ON_PLATFORM, PLAIN_IRI + "#" + platform);
		doorbell.addMetadata(OUTPUT_TYPE, OUTPUT_IRI);
		doorbell.addMetadata(LAST_EVENT_DATE_TIME, getTimeStamp());
		
		try {
			postRequest("http://localhost:4567/cats/root?href="+individualName,new StringEntity(new GsonBuilder().create().toJson(doorbell)));
		} catch (UnsupportedEncodingException e) {
			System.out.println ("UnsupportedEncodingException in Doorbell.registerToServer: " + e);
		}
		
		return doorbell;
	}

	@Override
	public String toString() {
		return "Doorbell [lastEvent=" + lastEvent + ", platform=" + platform
				+ ", individualName=" + individualName + "]";
	}
	
	 public void run() {
		 Scanner in = new Scanner(System.in);
		 String s = null;
		 
		 System.out.println ("Registering to the server ...");
		 System.out.println (registerToServer());
		 
		 System.out.println("Press \"r\" to ring the door.\nPress \"q\" to quit.\n");
		 
		 while (true) {
		      s = in.nextLine();
		      
		      if (s != null && !s.isEmpty()){
		    	  if (s.equals("r")){
		    		  ringTheDoor();
		    		  System.out.println ("Door is ringing: " + sendSignal());
		    	  } else if (s.equals("q")) {
		    		  System.out.println ("Quitting doorbell ...");
		    		  break;
		    	  }
		      } else{
		    	  break;
		      }
		 }
		 
		 in.close();
		 System.out.println ("Doorbell has stopped working.");
	 }
	 
	 private String getTimeStamp(){
		 TimeZone tz = TimeZone.getTimeZone("UTC");
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		 df.setTimeZone(tz);
		 return df.format(new Date());
	 }
	 
	 private boolean postRequest (String url, HttpEntity entity){
		 HttpPost request = new HttpPost(url);
		 try {
			 request.setHeader("Content-Type", "application/json");
			 request.setEntity(entity);
			 
			 HttpResponse response = client.execute(request);
			 
			 if (response.getStatusLine().getStatusCode() == 201)
				 return true;
			
		 }catch (IOException e) {
			 System.out.println ("An IOException has occured in postRequest." + e.getMessage());
		 }finally {
			 request.releaseConnection();
		 }
		 return false;
	 }
}

package com.ozu.hypercat.demo;

public class StartDoorbell {

	public static void main(String[] args) {
		Doorbell doorbell = new Doorbell("Front Door", "Front Door Bell");

		try {
			doorbell.ringTheDoor();
			doorbell.start();
			doorbell.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println (doorbell);
	}

}

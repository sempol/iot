package com.ozu.hypercat.ontology;

import static com.ozu.hypercat.ontology.OntologyConstants.IS_PRODUCED_BY;
import static com.ozu.hypercat.ontology.OntologyConstants.LAST_EVENT_DATE_TIME;
import static com.ozu.hypercat.ontology.OntologyConstants.OUTPUT_TYPE;
import static com.ozu.hypercat.ontology.OntologyConstants.PRODUCED_OUTPUT;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.HttpStatus;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import spark.Request;

import com.ozu.hypercat.json.deserializers.JsonHypercatConverter;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.HypercatTriple;

public class OntologyManager {
	private static final String HAS_DESCRIPTION = "urn:X-tsbiot:rels:hasDescription:en";
	private String ontPath = "sspn-ql-rdf.owl";
	
	private OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	private OWLOntology ont = null;
	private OWLDataFactory factory = null;
	
	public OntologyManager () {
		try {
			ont = manager.loadOntologyFromOntologyDocument(new File(ontPath));
		} catch (OWLOntologyCreationException e) {
	    	System.out.println ("Ontology Creation Exception: "+e.getMessage());
		}
        factory = manager.getOWLDataFactory();
	}
	
	public int addOrUpdateSensor (Request request){
		// TODO Check if the sensor is modified (new class axioms or object/data properties)
	    int responseStatus = HttpStatus.SC_CREATED;
		HypercatItem item = new JsonHypercatConverter().jsonToHypercat(request.body().toString(), request.queryParams("href"));
		String plainIRIString = extractSensorIRI(item);
		
		if (item == null || plainIRIString == null || plainIRIString.isEmpty())
			return HttpStatus.SC_BAD_REQUEST;
	    
	    try {
			IRI sensorIRI = IRI.create(plainIRIString + "#" + item.getHref());
			/* Create Individual */
	        OWLNamedIndividual sensor = factory.getOWLNamedIndividual(sensorIRI);
		    
			/* If ontology does not contain the individual, it is a new entry 
			 * Else, we have to extract the output data */
		    if (ont.containsIndividualInSignature(sensorIRI)){
		        /* Check sensing (output) data first */
		    	if (addSensorOutput(item, sensor, plainIRIString))
		    		responseStatus = HttpStatus.SC_OK;
		    	else
		    		responseStatus = HttpStatus.SC_NO_CONTENT;
		    }
		    
		    /* Add Class Assertions */
	        addClassAxioms(item, sensor, sensorIRI.toString());
	        
	        /* Add Object and Data Properties */
	        addObjectDataProperties(item, sensor);

		    manager.saveOntology (ont, new FileOutputStream (new File (ontPath)));
	    } catch (OWLOntologyStorageException e) {
	    	responseStatus = HttpStatus.SC_BAD_REQUEST;
	    	e.printStackTrace();
	    } catch (FileNotFoundException e) {
	    	responseStatus = HttpStatus.SC_BAD_REQUEST;
	    	e.printStackTrace();
	    }
	    return responseStatus;
	}
	
	private boolean addSensorOutput (HypercatItem item, OWLNamedIndividual sensor, String plainIriString){
		// TODO Dummy implementation. In addition, outputType is assumed to have same IRI prefix as the sensor!
		try {
			HypercatTriple outputType = item.getMetadata().stream().filter(i -> i.getRel().equals(OUTPUT_TYPE)).findFirst().get();
			HypercatTriple lastEvent = item.getMetadata().stream().filter(i -> i.getRel().equals(LAST_EVENT_DATE_TIME)).findFirst().get();

			OWLClass outputClass = factory.getOWLClass(IRI.create(outputType.getVal()));
			OWLNamedIndividual output = factory.getOWLNamedIndividual(IRI.create(plainIriString + "#" + item.getHref()
					+ "Output" + lastEvent.getVal().replace(" ", "").replace(":", "")));
			OWLClassAssertionAxiom outputClassAxiom = factory.getOWLClassAssertionAxiom(outputClass, output);

			manager.addAxiom(ont, outputClassAxiom);
			item.removeMetadata(outputType);

			OWLDataProperty hasEventDateTime = factory.getOWLDataProperty(IRI.create(LAST_EVENT_DATE_TIME));
			OWLDataPropertyAssertionAxiom dataPropertyAxiom = factory
					.getOWLDataPropertyAssertionAxiom(hasEventDateTime, output, lastEvent.getVal());

			manager.addAxiom(ont, dataPropertyAxiom);
			item.removeMetadata(lastEvent);

			OWLObjectProperty isProducedBy = factory.getOWLObjectProperty(IRI.create(IS_PRODUCED_BY));
			OWLObjectPropertyAssertionAxiom objectPropertyAxiom = factory.getOWLObjectPropertyAssertionAxiom(
					isProducedBy, output, sensor);

			manager.addAxiom(ont, objectPropertyAxiom);

			OWLObjectProperty producedOutput = factory.getOWLObjectProperty(IRI.create(PRODUCED_OUTPUT));
			OWLObjectPropertyAssertionAxiom producedOutputAxiom = factory.getOWLObjectPropertyAssertionAxiom(
					producedOutput, sensor, output);

			manager.addAxiom(ont, producedOutputAxiom);
		} catch (NoSuchElementException ex){
	    	System.out.println ("OntologyManager could not find Output Type or Last Event Date");
	    	return false;
		} catch (Exception e){
	    	e.printStackTrace();
	    	return false;
		} 
        return true;
	}
	
	private boolean addClassAxioms (HypercatItem item, OWLNamedIndividual sensor, String sensorIRI) {
		List<HypercatTriple> classAssertions = extractClassAssertions(item, sensorIRI);
		
		if (classAssertions == null || classAssertions.isEmpty())
			return false;
		
        for (HypercatTriple c : classAssertions){
	        OWLClass sensorClass = factory.getOWLClass(IRI.create(c.getRel()));
	        OWLClassAssertionAxiom sensorClassAxiom = factory.getOWLClassAssertionAxiom(sensorClass, sensor);
	        
	        manager.addAxiom(ont, sensorClassAxiom);
	        item.removeMetadata(c);
        }
        
        return true;
	}
	
	private boolean addObjectDataProperties (HypercatItem item, OWLNamedIndividual sensor) {
		Set<HypercatTriple> properties = item.getMetadata();
		
		if (properties == null || properties.isEmpty())
			return false;
		
        for (HypercatTriple p : properties){
        	if (isObjectProperty(p.getVal())){
		        OWLNamedIndividual targetObject = factory.getOWLNamedIndividual(IRI.create(p.getVal()));
        		OWLObjectProperty objectProperty = factory.getOWLObjectProperty(IRI.create(p.getRel()));
		        OWLObjectPropertyAssertionAxiom objectPropertyAxiom = factory.getOWLObjectPropertyAssertionAxiom(
						objectProperty, sensor, targetObject);
		        
		        manager.addAxiom(ont, objectPropertyAxiom);
        	}else {
        		OWLDataProperty dataProperty = factory.getOWLDataProperty(IRI.create(p.getRel()));
		        OWLDataPropertyAssertionAxiom dataPropertyAxiom = factory
		                .getOWLDataPropertyAssertionAxiom(dataProperty, sensor, p.getVal());
		        
		        manager.addAxiom(ont, dataPropertyAxiom);
        	}
        	item.removeMetadata(p);
        }
        
        return true;
	}
	
	private boolean isObjectProperty (String value){ // if false it is a dataProperty
		// TODO Properly check if this is a valid IRI or a resource for an individual
		return value.contains("http") || value.contains("https");
	}
	
	private List<HypercatTriple> extractClassAssertions (HypercatItem item, String sensorIRI){
		try {
			List<HypercatTriple> classAssertions =  item.getMetadata().stream().filter(i -> i.getVal().equals(sensorIRI)).collect(Collectors.toList());
			return classAssertions;
		} catch (NoSuchElementException ex){
	    	System.out.println ("OntologyManager could not find class assertion axioms");
	    	return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String extractSensorIRI (HypercatItem item){
		// TODO null check ?!
		try {
			HypercatTriple triple =  item.getMetadata().stream().filter(i -> i.getRel().equals(HAS_DESCRIPTION)).findFirst().get();
			item.removeMetadata(triple);
			return triple.getVal();
		} catch (NoSuchElementException ex){
	    	System.out.println ("OntologyManager could not find individual's ontology IRI (plainIRI)");
	    	return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

package com.ozu.hypercat.server;

import static com.ozu.hypercat.objects.Rel.CAT;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.delete;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import spark.Request;
import spark.Response;

import com.google.gson.GsonBuilder;
import com.ozu.hypercat.json.deserializers.CatalogueDeserializer;
import com.ozu.hypercat.json.deserializers.ItemDeserializer;
import com.ozu.hypercat.objects.HypercatCatalogue;
import com.ozu.hypercat.objects.HypercatItem;
import com.ozu.hypercat.objects.HypercatObject;
import com.ozu.knowledgebase.KnowledgeBaseManager;
import com.ozu.knowledgebase.hypercat.commands.DeleteInformation;
import com.ozu.knowledgebase.hypercat.commands.NewInformation;
import com.ozu.knowledgebase.hypercat.commands.UpdateInformation;

public class Server {

	private static HypercatCatalogue cats;

	private static GsonBuilder gsonBuilderItem;
	private static GsonBuilder gsonBuilderCatalogue;
	private static KnowledgeBaseManager kbManager;

	static {
		cats = new HypercatCatalogue("Root Catalogue");
		cats.setPathString(CAT);
		cats.setHref("/" + CAT);

		gsonBuilderItem = new GsonBuilder();
		gsonBuilderItem.excludeFieldsWithoutExposeAnnotation();
		gsonBuilderItem.registerTypeAdapter(HypercatItem.class, new ItemDeserializer());

		gsonBuilderCatalogue = new GsonBuilder();
		gsonBuilderCatalogue.registerTypeAdapter(HypercatCatalogue.class, new CatalogueDeserializer());

		try {
			kbManager = KnowledgeBaseManager.getInstance();
		} catch (Exception e) {
			System.err.println("Error while creating KnowledgeBaseManager: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		get("/cats", (request, response) -> {
			return handleGet(request, response);
		} , new JsonTransformer());

		get("/cats/*", "application/json", (request, response) -> {
			return handleGet(request, response);
		} , new JsonTransformer());

		post("/cats", (request, response) -> {
			return handlePost(request, response);
		} , new JsonTransformer());

		post("/cats/*", "application/json", (request, response) -> {
			return handlePost(request, response);
		} , new JsonTransformer());

		put("/cats", (request, response) -> {
			return handleUpdate(request, response);
		} , new JsonTransformer());

		put("/cats/*", "application/json", (request, response) -> {
			return handleUpdate(request, response);
		} , new JsonTransformer());

		delete("/cats", (request, response) -> {
			return handleDelete(request, response);
		} , new JsonTransformer());

		delete("/cats/*", "application/json", (request, response) -> {
			return handleDelete(request, response);
		} , new JsonTransformer());
		
		get("/new_event", (request, response) -> {
			return "In order to send a new event, please make a POST request to this URL. The body should be in Hypercat Item format and it should contain all required triples.";
		} , new JsonTransformer());
		
		post("/new_event", (request, response) -> {
			return handleNewEvent(request, response);
		} , new JsonTransformer());

	}

	/**
	 * @param cat
	 *            to search in
	 * @param path
	 *            is in the form of /cats/building1/room1
	 * @return HypercatCatalogue
	 */
	public static HypercatCatalogue findTargetObject(HypercatCatalogue cat, String[] rest) {
		return findTargetObject(cat, CAT, rest);
	}

	public static HypercatCatalogue findTargetObject(HypercatCatalogue cat, String myPath, String[] rest) {
		if (myPath.equals(cat.getPathString()) && (rest.length == 0)) {
			return cat;
		} else {
			List<HypercatObject> items = cat.getItems();
			for (HypercatObject object : items) {
				if (object instanceof HypercatCatalogue) {
					if (((HypercatCatalogue) object).getPathString().equals(rest[0])) {
						return findTargetObject(((HypercatCatalogue) object), rest[0],
								(String[]) ArrayUtils.remove(rest, 0));
					}
				}
			}
			// Catalogue Not Found define new

			HypercatCatalogue catalogue = new HypercatCatalogue();
			catalogue.addDescription(rest[0]);
			catalogue.setHref(cat.getHref() + "/" + rest[0]);
			catalogue.setPathString(rest[0]);

			cat.addItem(catalogue);

			return findTargetObject(catalogue, rest[0], (String[]) ArrayUtils.remove(rest, 0));
		}
	}
	
	private static HypercatObject handleGet(Request request, Response response) {
		boolean handleItem = request.queryParams().contains("href");
		HypercatCatalogue catalogue = findTargetCatalogue(request);

		if (catalogue == null) {
			response.status(404);
			return null;
		}
		if (handleItem == true) {
			return catalogue.getItem(request.params("href"));
		} else {
			return catalogue;
		}
	}

	private static HypercatObject handleCRUD(Request request, Response response, boolean update, boolean isDelete) {
		boolean handleItem = request.queryParams().contains("href");
		HypercatCatalogue catalogue = findTargetCatalogue(request);

		if (catalogue == null) {
			response.status(404);
			return null;
		}

		if (handleItem == true) {
			return handleItem(request, response, catalogue, update, isDelete);
		} else {
			// throw new RuntimeException("OUT OF SCOPE");
			return handleCatalogue(request, response, catalogue, update, isDelete);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @param catalogue
	 * @param update
	 * @param isDelete
	 * @return
	 */
	private static HypercatObject handleCatalogue(Request request, Response response, HypercatCatalogue catalogue,
			boolean update, boolean isDelete) {
		HypercatCatalogue newCatalogue = (gsonBuilderCatalogue.create()).fromJson(request.body().toString(),
				HypercatCatalogue.class);

		String[] splat = request.splat();
		newCatalogue.setPathString(splat[splat.length - 1]);
		newCatalogue.setHref(StringUtils.join(splat, "/"));

		handleObject(request, response, catalogue, update, isDelete, newCatalogue);
		return newCatalogue;
	}

	private static HypercatObject handleItem(Request request, Response response, HypercatCatalogue catalogue,
			boolean update, boolean isDelete) {

		HypercatItem item = (gsonBuilderItem.create()).fromJson(request.body().toString(), HypercatItem.class);
		return handleObject(request, response, catalogue, update, isDelete, item);
	}

	private static HypercatObject handleObject(Request request, Response response, HypercatCatalogue catalogue,
			boolean update, boolean isDelete, HypercatObject object) {
		if (object instanceof HypercatItem && !request.queryParams("href").equals(object.getHref())) {
			response.status(409);
			return null;
		} else {
			if (isDelete == true) {
				catalogue.removeItem(object.getHref());
				kbManager.processHypercatCommand(new DeleteInformation(object));
			} else {
				HypercatObject prev = catalogue.getItem(object.getHref());
				if (prev == null && update == true)
					response.status(409);
				if (prev != null && object != null && update == true)
					kbManager.processHypercatCommand(new UpdateInformation(object, prev));
				else if (object != null && update == false)
					kbManager.processHypercatCommand(new NewInformation(object));
				catalogue.addItem(object, true);
			}
			return object;
		}
	}

	private static HypercatCatalogue findTargetCatalogue(Request request) {
		String[] splat = new String[0];

		if (request.splat() != null && request.splat().length > 0)
			splat = request.splat()[0].split("/");

		HypercatCatalogue catalogue = findTargetObject(cats, splat);
		return catalogue;
	}

	private static HypercatObject handlePost(Request request, Response response) {
		return handleCRUD(request, response, false, false);
	}

	private static HypercatObject handleUpdate(Request request, Response response) {
		return handleCRUD(request, response, true, false);
	}

	private static HypercatObject handleDelete(Request request, Response response) {
		return handleCRUD(request, response, false, true);
	}
	
	private static HypercatObject handleNewEvent(Request request, Response response){
		HypercatItem item = (gsonBuilderItem.create()).fromJson(request.body().toString(), HypercatItem.class);
		kbManager.processHypercatCommand(new NewInformation(item));
		return item;
	}
}
